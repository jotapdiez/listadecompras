package org.jotapdiez.listadecompras;

import java.util.Arrays;
import java.util.List;

import org.jotapdiez.listadecompras.model.Producto;

public class Tests {

	public static void main(String[] args) {
		Producto prod1 = new Producto();
		prod1.setId("1");
		prod1.setNombre("prod1");
		Producto prod2 = new Producto();
		prod2.setId("2");
		prod2.setNombre("prod2");
		List<Producto> prods = Arrays.asList(prod1, prod2);
		
		Producto prodTest = new Producto();
		prodTest.setId("2");
		int index = prods.indexOf(prodTest);
		System.out.println(index);
		System.out.println(prods.contains(prodTest));
		
		prodTest.setId("2");
		prod2.setNombre("prod2a");
		System.out.println("prods: " + prods.size());
		System.out.println("org: " + prods.set(index, prodTest).getNombre());
		System.out.println("mod: " + prods.get(index).getNombre());
		System.out.println("prods: " + prods.size());
	}

}

package org.jotapdiez.listadecompras.configuration;

import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
//@EnableTransactionManagement
//@ComponentScan({ "org.jotapdiez.listadecompras.controller",
//				"org.jotapdiez.listadecompras.model",
//				"org.jotapdiez.listadecompras.service",
//				"org.jotapdiez.listadecompras.dao"
//			})
////@PropertySource(value = { "classpath:hibernate.properties" })
@EnableMongoRepositories(basePackages = "org.jotapdiez.listadecompras.repository")
public class ServerConfig {

//     @Autowired
//     private Environment environment;

//     @Bean
//     public LocalSessionFactoryBean sessionFactory() {
//         LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
//         sessionFactory.setDataSource(dataSource());
//         sessionFactory.setPackagesToScan(new String[] { "org.jotapdiez.listadecompras.model" });
//         sessionFactory.setHibernateProperties(hibernateProperties());
// //        sessionFactory.setImplicitNamingStrategy(new CustomPhysicalNamingStrategy());
// //        sessionFactory.setPhysicalNamingStrategy(new CustomPhysicalNamingStrategy());
//         return sessionFactory;
//      }

//     @Bean
//     public DataSource dataSource() {

//         DriverManagerDataSource dataSource = new DriverManagerDataSource();
//         dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
//         dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
//         dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
//         dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));

//         return dataSource;
//     }

//     private Properties hibernateProperties() {
//         Properties properties = new Properties();
//         properties.put(AvailableSettings.DIALECT, environment.getRequiredProperty("hibernate.dialect"));
//         properties.put(AvailableSettings.SHOW_SQL, environment.getRequiredProperty("hibernate.show_sql"));
//         properties.put(AvailableSettings.FORMAT_SQL, environment.getRequiredProperty("hibernate.format_sql"));
//         properties.put(AvailableSettings.GLOBALLY_QUOTED_IDENTIFIERS, true);
//         properties.put("hibernate.connection.characterEncoding", "utf8");
//         properties.put("hibernate.connection.CharSet", "utf8");
//         properties.put("hibernate.connection.useUnicode", true);
//         return properties;
//     }

//     @Bean
//     @Autowired
//     public HibernateTransactionManager transactionManager(SessionFactory s) {
//        HibernateTransactionManager txManager = new HibernateTransactionManager();
//        txManager.setSessionFactory(s);
//        return txManager;
//     }
}

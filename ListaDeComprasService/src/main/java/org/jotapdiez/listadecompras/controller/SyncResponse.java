package org.jotapdiez.listadecompras.controller;

public class SyncResponse{

	MergeResult result = null;
	String message = "";
	
	public SyncResponse(MergeResult mergeResult){
		this.result = mergeResult;
	}

	public String getStatus() {
		return message;
	}
	public void setStatus(String status) {
		this.message = status;
	}
	
	public MergeResult getResult() {
		return result;
	}
	public void setResult(MergeResult result) {
		this.result = result;
	}
}
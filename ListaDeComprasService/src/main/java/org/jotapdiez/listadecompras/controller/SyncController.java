package org.jotapdiez.listadecompras.controller;

import org.jotapdiez.listadecompras.model.Store;
import org.jotapdiez.listadecompras.service.ISyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController
public class SyncController {

	@Autowired
	ISyncService service;

	@GetMapping(value = "/syncBack")
	public Store load() {
		Store res = service.getCurrentStore();
		return res;
	}
	
	@PostMapping(value = "/sync")
	public SyncResponse sync(@RequestBody Store store) {
		MergeResult mergeResult = service.merge(store);
		return new SyncResponse(mergeResult);
	}
}

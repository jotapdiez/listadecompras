package org.jotapdiez.listadecompras.controller;

import java.util.Date;
import java.util.Optional;

import org.jotapdiez.listadecompras.model.Store;

public class MergeResult {
	private boolean changed = false;
	
	private MergePair productos = new MergePair();
	private MergePair tiendas = new MergePair();
	private MergePair precios = new MergePair();
	private MergePair listas = new MergePair();
	
	private Store store = null;
	
	public MergeResult(Store oldStore, Optional<Date> storeDate) {
		store = Store.clone(oldStore);
		storeDate.ifPresent(date->store.setDate(date));
	}
	
	public void setChanged(boolean changed) {
		this.changed = changed;
	}
	public boolean isChanged() {
		return changed;
	}
}
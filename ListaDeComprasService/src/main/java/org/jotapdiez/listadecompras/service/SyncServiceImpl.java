package org.jotapdiez.listadecompras.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jotapdiez.listadecompras.controller.MergeResult;
import org.jotapdiez.listadecompras.model.Elemento;
import org.jotapdiez.listadecompras.model.ListaCompras;
import org.jotapdiez.listadecompras.model.Precio;
import org.jotapdiez.listadecompras.model.Producto;
import org.jotapdiez.listadecompras.model.Store;
import org.jotapdiez.listadecompras.model.Tienda;
import org.jotapdiez.listadecompras.repository.IStoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SyncServiceImpl implements ISyncService {

	@Autowired
	IStoreRepository storeRepo;

	public Store getCurrentStore() {
		Optional<Store> prev = storeRepo.findTopByOrderByDateDesc();
		
		Store currentStore = null;
		if (!prev.isPresent()) {
			currentStore = new Store();
			currentStore.setDate(new Date());
			storeRepo.save(currentStore);
		}else {
			currentStore = prev.get();
		}
		
		return currentStore;
	}
	
	public synchronized MergeResult merge(Store store) {
		Optional<Store> prev = storeRepo.findTopByOrderByDateDesc();
		if (!prev.isPresent()) {
			store.setDate(new Date());
			storeRepo.save(store);
			
			MergeResult result = new MergeResult(store, Optional.empty());
//			result.store = store;
			
			return result;
		}
		
		Store prevStore = prev.get();
		MergeResult result = new MergeResult(Store.clone(prevStore), Optional.of(new Date()));
//		result.store = Store.clone(prevStore);
//		result.store.setDate(new Date());

		Store newStore = result.store;
		
		Date lastDate = prevStore.getDate();

		{ // Productos
			Set<Producto> fromMerged = newStore.getProductos();
			List<Producto> nuevos = filterNew(store.getProductos().parallelStream(), lastDate);
			fromMerged.addAll(nuevos);
			
			List<Producto> actualizados = filterUpdates(store.getProductos().parallelStream(), nuevos, lastDate);
			actualizados.stream().forEach(i->{
				fromMerged.remove(i);
				fromMerged.add(i);
			});
			
			System.out.println("Productos nuevos: " + nuevos.size());
			System.out.println("Productos actualizados: " + actualizados.size());
			result.productos.news = nuevos.size();
			result.productos.updates = actualizados.size();
//			if (nuevos.size()>0 || actualizados.size()>0) {
//				result.algunCambio = true;
//			}
			result.setChanged(nuevos.size()>0 || actualizados.size()>0);
		}
		
		{ // Tiendas
			Set<Tienda> fromMerged = newStore.getTiendas();
			List<Tienda> nuevos = filterNew(store.getTiendas().stream(), lastDate);
			fromMerged.addAll(nuevos);
			
			List<Tienda> actualizados = filterUpdates(store.getTiendas().parallelStream(), nuevos, lastDate);
			actualizados.stream().forEach(i->{
				fromMerged.remove(i);
				fromMerged.add(i);
			});
			
			System.out.println("Tiendas nuevos: " + nuevos.size());
			System.out.println("Tiendas actualizados: " + actualizados.size());
			result.tiendas.news = nuevos.size();
			result.tiendas.updates = actualizados.size();
//			if (nuevos.size()>0 || actualizados.size()>0) {
				result.setChanged(nuevos.size()>0 || actualizados.size()>0);
//			}
		}
		
		{ // Precios
			Set<Precio> fromMerged = newStore.getPrecios();
			List<Precio> nuevos = filterNew(store.getPrecios().stream(), lastDate);
			fromMerged.addAll(nuevos);
			
//			List<Precio> actualizados = filterUpdates(store.getPrecios().parallelStream(), nuevos, lastDate);
//			actualizados.stream().forEach(i->{
//				fromMerged.remove(i);
//				fromMerged.add(i);
//			});
			
			System.out.println("Precios nuevos: " + nuevos.size());
			result.precios.news = nuevos.size();
//			result.precios.updates = actualizados.size();
//			if (nuevos.size()>0) {
//				result.algunCambio = true;
//			}
			result.setChanged(nuevos.size()>0);
		}
		
		{ // Compras
			Set<ListaCompras> fromMerged = newStore.getCompras();
			List<ListaCompras> nuevos = filterNew(store.getCompras().stream(), lastDate);
			fromMerged.addAll(nuevos);
			
			List<ListaCompras> actualizados = filterUpdates(store.getCompras().parallelStream(), nuevos, lastDate);
			actualizados.stream().forEach(i->{
				fromMerged.remove(i);
				fromMerged.add(i);
			});
			
			System.out.println("ListaCompras nuevas: " + nuevos.size());
			System.out.println("ListaCompras actualizadas: " + actualizados.size());
			result.listas.news = nuevos.size();
			result.listas.updates = actualizados.size();
//			if (nuevos.size()>0 || actualizados.size()>0) {
//				result.algunCambio = true;
//			}
			result.setChanged(nuevos.size()>0 || actualizados.size()>0);
		}

		storeRepo.save(newStore);
		
		return result;
	}
	
	private <T extends Elemento> List<T> filterNew(Stream<T> stream, Date lastDate){
		return stream.filter(p->p.getCreateDate() != null && p.getCreateDate().after(lastDate))
				.collect(Collectors.toList());
	}
	
	private <T extends Elemento> List<T> filterUpdates(Stream<T> stream, List<T> exclude, Date lastDate){
		return stream.filter(p->
								p.getUpdateDate() != null 
							&&  p.getUpdateDate().after(lastDate) 
							&&  !exclude.contains(p)
						)
				.collect(Collectors.toList());
	}
}

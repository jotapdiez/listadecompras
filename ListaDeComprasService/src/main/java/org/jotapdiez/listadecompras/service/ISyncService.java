package org.jotapdiez.listadecompras.service;

import org.jotapdiez.listadecompras.controller.MergeResult;
import org.jotapdiez.listadecompras.model.Store;

public interface ISyncService {

	public Store getCurrentStore();
	public MergeResult merge(Store store);
}

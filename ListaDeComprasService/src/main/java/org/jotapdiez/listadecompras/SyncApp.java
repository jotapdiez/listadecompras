package org.jotapdiez.listadecompras;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
//@EnableDiscoveryClient
//@ComponentScan({ "org.jotapdiez.listadecompras.configuration"})
//@PropertySource(value = { "classpath:application.properties" })
public class SyncApp {
    public static void main( String[] args )
    {
//    	System.setProperty("spring.config.name", "access-service");
        SpringApplication.run(SyncApp.class, args);
    }
}

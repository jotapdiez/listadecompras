package org.jotapdiez.listadecompras.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Precios")
public class Precio extends Elemento {

/*
{
        id:1,
        tienda:3,
        producto:1,
        valor:65,
        moneda:'$',
        fechaCarga: '2019-05-23T16:08:09.541Z',
        fechaActualizacion:null
    }
*/

	private String tienda;
	private String producto;
    private double valor = 0;
    private String moneda = "$";
    private String oferta = null;
    private String tipoOferta = null;

    public void setProducto(String producto) {
    	this.producto = producto;
    }
    public String getProducto() {
    	return producto;
    }
    
    public String getMoneda() {
		return moneda;
	}
    public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
    public void setTienda(String tienda) {
    	this.tienda = tienda;
    }
    
    public String getTienda() {
    	return tienda;
    }

    public double getValor() {
        return valor;
    }
    public void setValor(double valor) { this.valor = valor; }
    
    public String getOferta() {
		return oferta;
	}
    public void setOferta(String oferta) {
		this.oferta = oferta;
	}
    public String getTipoOferta() {
    	return tipoOferta;
    }
    public void setTipoOferta(String tipoOferta) {
    	this.tipoOferta = tipoOferta;
    }
}

package org.jotapdiez.listadecompras.model;

import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Store {
	public static Store clone(Store prev) {
		Store store = new Store();
		store.setProductos(prev.productos);
		store.setCompras(prev.compras);
		store.setPrecios(prev.precios);
		store.setTiendas(prev.tiendas);
		
		return store;
	}

	Set<Precio> precios;
    Set<Tienda> tiendas;
    Set<ListaCompras> compras;
    Set<Producto> productos;
    
    @JsonIgnore
    private Date date = null;
    
	public Set<Precio> getPrecios() {
		return precios;
	}
	public void setPrecios(Set<Precio> precios) {
		this.precios = precios;
	}
	public Set<Tienda> getTiendas() {
		return tiendas;
	}
	public void setTiendas(Set<Tienda> tiendas) {
		this.tiendas = tiendas;
	}
	public Set<ListaCompras> getCompras() {
		return compras;
	}
	public void setCompras(Set<ListaCompras> compras) {
		this.compras = compras;
	}
	public Set<Producto> getProductos() {
		return productos;
	}
	public void setProductos(Set<Producto> productos) {
		this.productos = productos;
	}

    public Date getDate() {
		return date;
	}
    public void setDate(Date date) {
		this.date = date;
	}
    
    @Override
    public String toString() {
    	return  "Productos:    " + productos +
    		    "\nPrecios:      " + precios + 
    			"\nTiendas:      " + tiendas + 
    			"\nListasCompra: " + compras;
    }
}

package org.jotapdiez.listadecompras.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Tiendas")
public class Tienda extends Elemento {

/*
{
    "id": "asdsa-asdas-dasd-asd",
    "pais": 'Argentina',
    "ciudad": 'San Martin',
    "nombre": 'Carrefour',
    "direccion": 'Av. San Martín 420'
}
*/

	private String pais = "";
	private String ciudad = "";
	private String nombre = "";
	private String direccion = "";

	public Tienda() {
		
	}
	
	public String getNombre() { return nombre; }
	public void setNombre(String nombre) { this.nombre = nombre; }

	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Override
	public String toString() {
		return nombre + (ciudad == null || ciudad.isEmpty() ? "" : " - " + ciudad) + (direccion == null || direccion.isEmpty() ? "" : " - " + direccion);
	}
}

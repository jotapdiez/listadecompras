package org.jotapdiez.listadecompras.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Productos")
public class Producto extends Elemento {

/*
	{
        id:1,
        nombre: 'Pepsi',
        cantidadUnidad: 2.25,
        unidad: 'l',
        barcode: '7791813423119',
        observaciones: 'obs...'
    }
*/

	private String nombre = "";
	private float cantidadUnidad = 0;
	private String unidad = "";
	private long barcode = 0;
	private String categoria = "";
	private String subcategoria = "";
	private String marca = "";
	private String origen = "";
	private String observaciones = "";
	
	public String getNombre() { return nombre; }
	public void setNombre(String nombre) { this.nombre = nombre; }

	public long getBarcode() {
		return barcode;
	}
	public void setBarcode(long barcode) {
		this.barcode = barcode;
	}

	public String getUnidad() {
		return unidad;
	}
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public float getCantidadUnidad() {
		return cantidadUnidad;
	}
	public void setCantidadUnidad(float cantidadUnidad) {
		this.cantidadUnidad = cantidadUnidad;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getSubcategoria() {
		return subcategoria;
	}
	public void setSubcategoria(String subcategoria) {
		this.subcategoria = subcategoria;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
}

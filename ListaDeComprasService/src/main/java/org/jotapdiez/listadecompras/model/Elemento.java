package org.jotapdiez.listadecompras.model;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Elemento {

	@Id
	private String id = null;
	
	private Date createDate = null;
	private Date updateDate = null;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Producto) || o == null) {
			return false;
		}
		Producto oo = (Producto)o;
		
		return oo.getId() != null && oo.getId().equals(getId());
	}
	
	@Override
	public int hashCode() {
		return getId().hashCode();
	}
	@Override
	public String toString() {
		return getId().toString();
	}
}

package org.jotapdiez.listadecompras.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ListaCompras")
public class ListaCompras extends Elemento {

	private String title = null;
	private List<ProductoListaCompras> productos;
	private Date fechaEstimadaCompra;
	private Integer importeCalculado;
	private Date ultimoCalculo;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<ProductoListaCompras> getProductos() {
		return productos;
	}
	public void setProductos(List<ProductoListaCompras> productos) {
		this.productos = productos;
	}
	public Date getFechaEstimadaCompra() {
		return fechaEstimadaCompra;
	}
	public void setFechaEstimadaCompra(Date fechaEstimadaCompra) {
		this.fechaEstimadaCompra = fechaEstimadaCompra;
	}
	public Integer getImporteCalculado() {
		return importeCalculado;
	}
	public void setImporteCalculado(Integer importeCalculado) {
		this.importeCalculado = importeCalculado;
	}
	public Date getUltimoCalculo() {
		return ultimoCalculo;
	}
	public void setUltimoCalculo(Date ultimoCalculo) {
		this.ultimoCalculo = ultimoCalculo;
	}
}
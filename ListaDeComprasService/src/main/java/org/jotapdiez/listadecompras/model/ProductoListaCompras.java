package org.jotapdiez.listadecompras.model;

import javax.persistence.Entity;

@Entity
public class ProductoListaCompras extends Elemento {

	private String nombre = "";
	
	public String getNombre() { return nombre; }
	public void setNombre(String nombre) { this.nombre = nombre; }

	public Integer cantidad = 1;
	public Tienda tiendaCalculada = null;
	public Integer comprados = 1;
	
	public Tienda getTiendaCalculada() {
		return tiendaCalculada;
	}
	public void setTiendaCalculada(Tienda tiendaCalculada) {
		this.tiendaCalculada = tiendaCalculada;
	}
	
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
	public Integer getComprados() {
		return comprados;
	}
	public void setComprados(Integer comprados) {
		this.comprados = comprados;
	}
}

package org.jotapdiez.listadecompras.repository;

//import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import org.jotapdiez.listadecompras.model.Store;

@Repository
public interface IStoreRepository extends MongoRepository<Store, String> {

	public Optional<Store> findTopByOrderByDateDesc();
    // public Customer findByFirstName(String firstName);
    // public List<Customer> findByLastName(String lastName);

}
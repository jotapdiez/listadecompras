import 'react-native-get-random-values';
import { PayloadAction, createSlice  } from '@reduxjs/toolkit';
import { AppState } from '../reducers';
import { Options } from '../reducers/types';

interface OptionsState {
    item: Options,
    status: string | null,
    error: string | null,
}

const initialState: OptionsState = {
    item: { defaultCity:'', defaultCountry:'', defaultStore:''} as Options,
    status: 'idle',
    error: null,
};

export const ENTITY = 'options';
export const OptionsSlice = createSlice({
    name: ENTITY,
    initialState: initialState,
    reducers: {
        init: (state, action) => {
            state.item = action.payload;
        },
        setServer: (state, action: PayloadAction<string>) => {
            state.item.server = action.payload;
        },
        setDefaultCountry: (state, action: PayloadAction<string>) => {
            state.item.defaultCountry = action.payload;
        },
        setDefaultCity: (state, action: PayloadAction<string>) => {
            state.item.defaultCity = action.payload;
        },
        setDefaultStore: (state, action: PayloadAction<string>) => {
            state.item.defaultStore = action.payload;
        },
    },
});

export const { init, setServer, setDefaultCountry, setDefaultCity, setDefaultStore } = OptionsSlice.actions;

export const getStatus = (state: AppState): string => state[ENTITY].status;
export const getOptions = (state: AppState): Options => state[ENTITY].item;

export default OptionsSlice.reducer;

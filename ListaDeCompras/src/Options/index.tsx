import React, { useEffect, useState } from 'react';
import AsyncStorageUtils from '../utils/AsyncStorageUtils';
import { Box, Button, HStack, Input, Select, Text, VStack, useToast } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useAppDispatch, useAppSelector } from '../reducers';
import { getOptions, setDefaultCity, setDefaultCountry, setDefaultStore } from './Slice';
import { selectAll as selectAllTiendas } from '../Tiendas/Slice';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

const SERVER_URL_HEROKU = 'https://lista-de-compras-plus.herokuapp.com';
const SERVER_URL_WIFI = 'http://192.168.137.1:8080';
const SERVER_URL_HOTSPOT = 'http://192.168.0.24:8080';

export enum OptionScreen {
  Options = 'Options',
}

export type OptionsStackParamList = {
  [OptionScreen.Options]: undefined;
};

type Props = NativeStackScreenProps<OptionsStackParamList, OptionScreen.Options>;
export default function OptionsComponent({ navigation }: Props) {
  const dispatch = useAppDispatch();
  const toast = useToast();
  const options = useAppSelector(getOptions);
  const tiendas = useAppSelector(selectAllTiendas);
  const [server, setServer] = useState(SERVER_URL_HEROKU);

  const doSync = () => {};

  useEffect(
    () =>
      navigation.setOptions({
        title: 'Options',
      }),
    [navigation],
  );

  // const doSyncPOSTA = () => {
  //     navigation.goBack();

  //     toast.show({title:'Iniciando Sincronizacion', placement: "top"});
  //     fetch(server + '/sync', {
  //         method: 'POST',
  //         headers: {
  //             Accept: 'application/json',
  //             'Content-Type': 'application/json',
  //         },
  //         body: JSON.stringify(AsyncStorageUtils.getFullStore()),
  //     }).then(res => res.json())
  //         .then(res => {
  //             // if (res.ok){
  //             //   toast.show({title:'Sincronizar ok', placement: "top"});
  //             // }
  //             const { productos, tiendas, precios, listas } = res.result;

  //             let msj = "";
  //             if (precios.news > 0) {
  //                 msj += precios.news + " precio(s) nuevo(s).\n";
  //             }
  //             if (productos.news > 0) {
  //                 msj += '\n' + productos.news + " producto(s) nuevo(s).\n";
  //             }
  //             if (productos.updates > 0) {
  //                 msj += productos.updates + " producto(s) actualizado(s).\n";
  //             }
  //             if (tiendas.news > 0) {
  //                 msj += '\n' + tiendas.news + " tienda(s) nueva(s).\n";
  //             }
  //             if (tiendas.updates > 0) {
  //                 msj += tiendas.updates + " tienda(s) actualizada(s).\n";
  //             }
  //             if (listas.news > 0) {
  //                 msj += '\n' + listas.news + " lista(s) nueva(s).\n";
  //             }
  //             if (listas.updates > 0) {
  //                 msj += listas.updates + " lista(s) actualizada(s).\n";
  //             }

  //             if (res.status !== '') {
  //                 msj = res.status;
  //             }

  //             if (msj == '') {
  //                 msj = 'No se actualizo nada.'
  //             }
  //             toast.show({title: 'Sincronizar ok\n' + msj, placement: 'top'});
  //         }).catch((error) => {
  //             toast.show({title: 'Sincronizar Error\n' + error, placement: 'top'});
  //         });
  // };

  const doSyncBack = () => {
    navigation.goBack();

    toast.show({ title: 'Iniciando carga', placement: 'top' });
    console.log('Options::doSyncBack');
    fetch(server + '/syncBack', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((res) => {
        const jsonStr = JSON.stringify(res);
        AsyncStorageUtils._saveFullStorage(jsonStr);
        // dispatch(refreshStore(res)); // TODO: reimplementar
        toast.show({ title: 'Cargar ok', placement: 'top' });
      })
      .catch((error) => {
        toast.show({ title: 'Cargar Error: ' + error, placement: 'top' });
      });
  };

  const handleDefaultCountryChange = (country: string) => {
    dispatch(setDefaultCountry(country));
  };

  const handleDefaultCityChange = (country: string) => {
    dispatch(setDefaultCity(country));
  };

  const handleDefaultStoreChange = (storeId: string) => {
    dispatch(setDefaultStore(storeId));
  };

  // console.log('Render Options');
  return (
    <>
      <Box pl={['1', '4']} pr={['1', '5']} p="2">
        <VStack space={2} p={2}>
          <Text>Server:</Text>
          <Select selectedValue={SERVER_URL_HEROKU} onValueChange={(host) => setServer(host)}>
            <Select.Item label="Heroku" value={SERVER_URL_HEROKU} />
            <Select.Item label="Hotspot" value={SERVER_URL_HOTSPOT} />
            <Select.Item label="Wifi casa" value={SERVER_URL_WIFI} />
          </Select>
        </VStack>
        <VStack space={2} p={2}>
          <Text>Default Store:</Text>
          <Select
            selectedValue={options.defaultStore ?? ''}
            onValueChange={(store) => handleDefaultStoreChange(store)}
          >
            {tiendas.map((tienda) => (
              <Select.Item key={tienda.id} label={tienda.nombre} value={tienda.id} />
            ))}
          </Select>
        </VStack>
        <VStack space={2} p={2}>
          <Text>Default Country:</Text>
          <Select
            selectedValue={options.defaultCountry}
            onValueChange={(country) => handleDefaultCountryChange(country)}
          >
            <Select.Item label="Argentina" value="argentina" />
            <Select.Item label="Colombia" value="colombia" />
            <Select.Item label="Estados Unidos" value="eeuu" />
          </Select>
        </VStack>
        <VStack p={2}>
          <Text>Default City:</Text>
          <Input
            testID="defaultCity"
            value={options.defaultCity}
            onChangeText={(city) => handleDefaultCityChange(city)}
          />
        </VStack>
        <HStack justifyContent={'space-around'} space={2} p={2}>
          <VStack space={2}>
            <Text>Sincronizar</Text>
            <Button
              onPress={doSync}
              leftIcon={<Icon name="cloud-upload" color="white" size={20} />}
              accessibilityLabel="Sincronizar"
            >
              Sincronizar
            </Button>
          </VStack>
          <VStack space={2}>
            <Text>Cargar</Text>
            <Button
              leftIcon={<Icon name="cloud-download" color="white" size={20} />}
              onPress={doSyncBack}
              accessibilityLabel="Cargar"
            >
              Cargar
            </Button>
          </VStack>
        </HStack>
      </Box>
    </>
  );
}

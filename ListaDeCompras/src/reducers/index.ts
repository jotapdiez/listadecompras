import { Action, combineReducers, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

import ProductSlice, {ENTITY as ProductEntity} from '../Productos/Slice';
import ShoppingListSlice, {ENTITY as ShoppingListEntity} from '../ListaCompras/Slice';
import StoreSlice, {ENTITY as StoreEntity} from '../Tiendas/Slice';
import PriceSlice, {ENTITY as PriceEntity} from '../Productos/Prices/Slice';
import BarcodeSlice, {ENTITY as BarcodeEntity} from './BarcodeSlice';
import OptionsSlice, {ENTITY as OptionsEntity} from '../Options/Slice';

export const rootReducer = combineReducers({
    [ProductEntity]: ProductSlice,
    [ShoppingListEntity]: ShoppingListSlice,
    [PriceEntity]: PriceSlice,
    [StoreEntity]: StoreSlice,
    [BarcodeEntity]: BarcodeSlice,
    [OptionsEntity]: OptionsSlice,
    // reduxState: StateReducer,
});

const store = configureStore({
    reducer: rootReducer,
});

// export type AppState = typeof store.getState;
export type AppState = any;
// export type AppState = ReturnType<typeof rootReducer>

export type AppDispatch = typeof store.dispatch;

export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    typeof rootReducer,
    unknown,
    Action<string>
>;

export const useAppDispatch = () => useDispatch<AppDispatch>();

export const useAppSelector: TypedUseSelectorHook<typeof rootReducer> = useSelector;

export default store;

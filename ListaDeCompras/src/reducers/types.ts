export enum Unidad {
    Gramos = 'g',
    Miligramos = 'mg',
    Kilogramos = 'kg',
    Mililitros = 'ml',
    Litros = 'l',
    Paquetes = 'p',
    Unidades = 'u',
}

export interface Product {
    id: string,
    nombre: string,
    cantidadUnidad: number,
    unidad: string,
    barcode: string,
    observaciones: string,
    categoria: string,
    subcategoria: string,
    marca: string,
    origen: string,
    createDate: Date,
    updateDate: Date,
    // comprados?: number, // Sacar?
}

export interface Price {
    id: string,
    tienda:any,
    producto:string,
    valor:string,
    moneda:string,
    oferta:boolean,
    tipoOferta:string,
    createDate: Date,
    updateDate: Date
}

export interface Store {
    id: string,
    pais: string,
    ciudad: string,
    nombre: string,
    direccion: string,
    createDate: Date,
    updateDate: Date
}

export interface ShoppingListItems {
    cantidad: number,
    comprados: number,
    product_id: string,
    product_label: string,
    price_id?: string,
    price_value?: number,
}

export const ShoppingListItemDefault: ShoppingListItems = {
    cantidad: 0,
    comprados: 0,
    product_id: '',
    product_label: '',
    // price_id: null,
    // price_value: null
};

export interface ShoppingList {
    id: string,
    title: string,
    productos: ShoppingListItems[],
    fechaEstimadaCompra:Date,
    importeCalculado: number,
    ultimoCalculo: number,
    createDate:Date,
    updateDate:Date,
}

export interface Options {
    server: string,
    defaultCountry: string,
    defaultCity: string,
    defaultStore: string,
}








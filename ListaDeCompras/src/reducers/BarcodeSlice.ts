import { createSlice  } from '@reduxjs/toolkit';
import { AppState } from '.';

type BarcodeStateType = {
    item: string | null,
    status: string | null,
    error: string | null,
}

const initialState: BarcodeStateType = {
    item: '',
    status: 'idle',
    error: null,
};

export const ENTITY = 'barcode';

export const BarcodeSlice = createSlice({
    name: ENTITY,
    initialState: initialState,
    reducers: {
        setBarcodeScanned: (state, action) => {
            state.item = action.payload;
        },
        clearBarcodeScanned: (state) => {
            state.item = null;
        },
    },
    // extraReducers(builder) {
    //     builder
    //     .addCase(fetch.pending, (state) => {
    //         state.status = 'loading'
    //     })
    //     .addCase(fetch.fulfilled, (state, action) => {
    //         state.status = 'done'
    //         state.items = action.payload;
    //     })
    //     .addCase(fetch.rejected, (state, action) => {
    //         state.status = 'failed'
    //         state.error = action.error.message
    //     })
    // }
});

export const { setBarcodeScanned, clearBarcodeScanned } = BarcodeSlice.actions;

export const getStatus = (state: AppState) => state[ENTITY].status;
export const getBarcodeScanned = (state: AppState) => state[ENTITY].item;

export default BarcodeSlice.reducer;

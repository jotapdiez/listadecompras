export const STORAGE_ID = '@LDCPlus';

export const TIENDAS_STATE_NAME = 'tiendas';
export const COMPRAS_STATE_NAME = 'compras';
export const PRECIOS_STATE_NAME = 'precios';

export const STORAGE_TIENDAS_ID = STORAGE_ID + ':tiendas';
export const STORAGE_COMPRAS_ID = STORAGE_ID + ':compras';
export const STORAGE_PRODUCTOS_ID = STORAGE_ID + ':productos';
export const STORAGE_PRECIOS_ID = STORAGE_ID + ':precios';


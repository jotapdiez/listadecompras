import React, { memo, useCallback, useEffect, useState } from 'react';
import { NativeBaseProvider } from 'native-base';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import { Provider } from 'react-redux';
// import { configureStore } from '@reduxjs/toolkit'

import Icon from 'react-native-vector-icons/FontAwesome';

import Productos from './Productos';
import Tiendas from './Tiendas';
import ListaCompras from './ListaCompras';
import Options, { OptionScreen, OptionsStackParamList } from './Options';
import SplashScreen from './components/SplashScreen';
import AsyncStorageUtils from './utils/AsyncStorageUtils';
import { useAppDispatch } from './reducers';
import store from './reducers';

import { ENTITY as ProductEntity, init as initProducts } from './Productos/Slice';
import { ENTITY as ShoppingListEntity, init as initShoppingLists } from './ListaCompras/Slice';
import { ENTITY as StoreEntity, init as initStores } from './Tiendas/Slice';
import { ENTITY as OptionsEntity, init as initOptions } from './Options/Slice';
import { ENTITY as PriceEntity, init as initPrices } from './Productos/Prices/Slice';
import { NativeBaseTheme, NavigatorTheme, ThemeColors } from './utils/theme';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

const AppStorageLoader = memo(function AppStorageLoader() {
  const dispatch = useAppDispatch();

  const [isStoreLoading, setIsStoreLoading] = useState(true);

  useEffect(() => {
    AsyncStorageUtils.clear(); // ***** DEBUG!!!!
    AsyncStorageUtils.load().then(currentStore => {

      dispatch(initProducts(currentStore[ProductEntity]));
      dispatch(initShoppingLists(currentStore[ShoppingListEntity]));
      dispatch(initStores(currentStore[StoreEntity]));
      dispatch(initPrices(currentStore[PriceEntity]));
      dispatch(initOptions(currentStore[OptionsEntity]));

      setIsStoreLoading(false);
    });
  }, [dispatch]);

  if (isStoreLoading) {
    return <SplashScreen />;
  }

  return <MainStackNavigator />;
});

export default function App({}) {
  return (
    <NavigationContainer theme={NavigatorTheme}>
      <NativeBaseProvider theme={NativeBaseTheme}>
        <Provider store={store}>
          <AppStorageLoader />
        </Provider>
      </NativeBaseProvider>
    </NavigationContainer>
  );
}

export type MainScreenStackParamList = {
  ['Tab']: undefined;
};

const Tab = createBottomTabNavigator();
type Props = NativeStackScreenProps<MainScreenStackParamList, 'Tab'>;
const MainScreenNavigator = memo(function MainScreenNavigator({}: Props) {
  const tabBarIcon = useCallback((route, focused, size) => {
    const customColor = focused ? ThemeColors.Action : ThemeColors.Text;
    if (route.name === 'Productos') {
      return <Icon name={'beer'} size={size} color={customColor} />;
    } else if (route.name === 'ListaCompras') {
      return <Icon name={'shopping-cart'} size={size} color={customColor} />;
    } else if (route.name === 'Tiendas') {
      return <Icon name={'map-marker'} size={size} color={customColor} />;
    }
  }, []);

  // console.log('Render MainScreenNavigator');
  return (
    <Tab.Navigator
      screenOptions={({}) => ({
        tabBarActiveTintColor: ThemeColors.Action,
        tabBarActiveBackgroundColor: ThemeColors.Secondary,
        tabBarInactiveBackgroundColor: ThemeColors.Secondary,
        tabBarInactiveTintColor: ThemeColors.Text,
        tabBarLabelStyle: {
          paddingBottom: 3,
        },
        headerShown: false,
        tabBarStyle: {
          borderWidth: 0,
          margin: 0,
          padding: 0,
          backgroundColor: ThemeColors.Secondary,
        },
      })}
    >
      <Tab.Screen
        name="Productos"
        component={Productos}
        options={{
          tabBarIcon: ({ focused, size }) => tabBarIcon({ name: 'Productos' }, focused, size),
        }}
      />
      <Tab.Screen
        name="ListaCompras"
        component={ListaCompras}
        options={{
          tabBarIcon: ({ focused, size }) => tabBarIcon({ name: 'ListaCompras' }, focused, size),
        }}
      />
      <Tab.Screen
        name="Tiendas"
        component={Tiendas}
        options={{
          tabBarIcon: ({ focused, size }) => tabBarIcon({ name: 'Tiendas' }, focused, size),
        }}
      />
    </Tab.Navigator>
  );
});

const Stack = createStackNavigator<OptionsStackParamList & MainScreenStackParamList>();
const MainStackNavigator = memo(function MainStackNavigator() {
  return (
    <SafeAreaProvider>
      <Stack.Navigator>
        <Stack.Group screenOptions={{ headerShown: false }}>
          <Stack.Screen name="Tab" component={MainScreenNavigator} />
        </Stack.Group>
        <Stack.Group
          screenOptions={{
            headerStyle: {
              backgroundColor: ThemeColors.Primary,
            },
            headerTintColor: ThemeColors.Text,
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        >
          <Stack.Screen name={OptionScreen.Options} component={Options} />
        </Stack.Group>
      </Stack.Navigator>
    </SafeAreaProvider>
  );
});


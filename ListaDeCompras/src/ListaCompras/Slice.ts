import { createSlice  } from '@reduxjs/toolkit';
import { AppState } from '../reducers';
import { ShoppingList } from '../reducers/types';
import { v4 as uuidv4 } from 'uuid';
import { ShoppingListItems } from '../reducers/types';

interface ShoppingListState {
    items: ShoppingList[],
    formItem: ShoppingList | null,
    formShoppingListItem: ShoppingListItems | null,
    status: string | null,
    error: string | null,
}

const initialState: ShoppingListState = {
    items: [] as ShoppingList[],
    formItem: null,
    formShoppingListItem: null,
    status: 'idle',
    error: null,
};

export const ENTITY = 'shopping_list';

const updateObject = (object: ShoppingList, updatedObject: ShoppingList) => {
    object.id = updatedObject.id;
    object.title = updatedObject.title;
    object.productos = updatedObject.productos;
    object.fechaEstimadaCompra = updatedObject.fechaEstimadaCompra;
    object.importeCalculado = updatedObject.importeCalculado;
    object.ultimoCalculo = updatedObject.ultimoCalculo;
    object.createDate = updatedObject.createDate;
    object.updateDate = updatedObject.updateDate;
};

export const ShoppingListSlice = createSlice({
    name: ENTITY,
    initialState: initialState,
    reducers: {
        init: (state, action) => {
            state.items = action.payload;
        },
        setFormItem: (state, action) => {
            // console.log('setFormItem::', action.payload);
            state.formItem = action.payload;
        },
        setFormShoppingListItem: (state, action) => {
            state.formShoppingListItem = action.payload;
        },
        addItem: (state, action) => {
            action.payload.id = uuidv4();
            state.items.push(action.payload);
        },
        updateItem: (state, action) => {
            state.status = 'done';
            const {id} = action.payload;
            const existingItem = state.items.find((item:ShoppingList) => item.id === id);
            if (existingItem) {
                updateObject(existingItem, action.payload);
            }
        },
        removeItem: (state, action) => {
            state.status = 'done';
            const {id} = action.payload;
            const existingItem = state.items.find((item:ShoppingList) => item.id === id) || {} as ShoppingList;
            const existingItemIndex = state.items.indexOf(existingItem);
            if (existingItemIndex > -1) {
                const deletedItems = state.items.splice(existingItemIndex, 1);
                console.log('ShoppingListSlice::removeItem=', deletedItems);
            }
        },
    },
});

export const { init, setFormItem, updateFormItem, addItem, updateItem, removeItem, setFormShoppingListItem, updateFormShoppingListItem } = ShoppingListSlice.actions;

export const getStatus = (state: AppState): string => state[ENTITY].status;
export const selectAll = (state: AppState): ShoppingList[] => state[ENTITY].items;
export const getFormItem = (state: AppState): ShoppingList => state[ENTITY].formItem;
export const getFormShoppingListItem = (state: AppState): ShoppingListItems => state[ENTITY].formShoppingListItem;
export const selectById = (state: AppState, itemId: string): ShoppingList => state[ENTITY].items.find((item: ShoppingList) => item.id === itemId);

export default ShoppingListSlice.reducer;

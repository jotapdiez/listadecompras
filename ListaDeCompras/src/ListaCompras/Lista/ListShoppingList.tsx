import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';
import { IconButton, Box, Pressable, HStack, Text, View } from 'native-base';
import { ShoppingList, ShoppingListItems } from '../../reducers/types';
import { setFormItem, selectAll } from '../Slice';
import { useAppDispatch, useAppSelector } from '../../reducers';
import { RowMap, SwipeListView, SwipeRow } from 'react-native-swipe-list-view';
import { ShoppingListStackParamList, ShoppingListScreens } from '../screens';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import ListItem from '../../components/ListItem';
import ListHiddenItems from '../../components/ListHiddenItems';
import { HeaderButtons, ThemeColors } from '../../utils/theme';

const LOG_HIERACHY = 'ListaCompras::Formulario - ';

type Props = NativeStackScreenProps<ShoppingListStackParamList, ShoppingListScreens.ShoppingListList>;
export default function ListaCompras({navigation}:Props) {
  const dispatch = useAppDispatch();
  const compras = useAppSelector(selectAll);
  const openRowRef = useRef<SwipeRow<ShoppingList> | null>(null);

  const mostrarFormulario = useCallback(() => {
    navigation.navigate(ShoppingListScreens.ShoppingListForm);
  }, [navigation]);

  const doAdd = useCallback(() => {
    dispatch(
      setFormItem({productos: [] as ShoppingListItems[]} as ShoppingList),
    );
    mostrarFormulario();
  }, [dispatch, mostrarFormulario]);

  const headerRightButtons = useMemo(
    () => (
      <HStack backgroundColor={HeaderButtons.BackgroundColor}>
        <IconButton
          icon={<Icon name="plus" color={HeaderButtons.Color} size={30} />}
          onPress={doAdd}
        />
      </HStack>
    ),
    [doAdd],
  );

  useEffect(() => {
    navigation.setOptions({
      title: '',
      headerRight: () => headerRightButtons,
    });
  }, [headerRightButtons, navigation]);

  const calcularCantidadYComprados = (item: ShoppingList) => {
    let res = {
      comprados: 0,
      cantidad: 0,
    };
    for (let i in item.productos) {
      let prod = item.productos[i];
      res.comprados += prod.comprados;
      res.cantidad += prod.cantidad;
    }

    return res;
  };

  const onPressItem = useCallback((item: ShoppingList) => {
    dispatch(setFormItem(Object.assign({}, item)));
    mostrarFormulario();
  }, [dispatch, mostrarFormulario]);

  const renderItem = useCallback(({item}) => {
    let importe = 0;
    if (item.ultimoCalculo) {
      importe = item.importeCalculado;
    }

    const prodInfo = calcularCantidadYComprados(item);
    const fechaEstimadaCompra = item.fechaEstimadaCompra
      ? moment(item.fechaEstimadaCompra).format('L')
      : '';

    return (
      <ListItem
        title={item.title}
        subtitle={`Comprados: ${prodInfo.comprados} de ${prodInfo.cantidad}`}
        rightMain={
          <Text color="dark.300" bold>
            {fechaEstimadaCompra}
          </Text>
        }
        rightSub={
          <Text color="light.400" fontSize="xs">
            {importe}
          </Text>
        }
      />
    );
  }, []);

  const renderItemEmptyList = useCallback(() => {
    return (
      <Box pl={['1', '4']} pr={['1', '5']} py="1">
        <Pressable
          rounded="8"
          overflow="hidden"
          borderWidth="1"
          borderColor="coolGray.300"
          shadow="3"
          bg="coolGray.100"
          p="5">
          <Text>No hay listas de compra</Text>
        </Pressable>
      </Box>
    );
  }, []);

  // const handleSearch = (text: string) => {
  //   console.log('handleSearch::text = ', text);
  // };

  const closeOpenRow = useCallback(() => {
    if (openRowRef.current && openRowRef.current.closeRow) {
      openRowRef.current.closeRow();
    }
  }, []);

  const renderHiddenItem = useCallback(({item}) => {
    const icons = [
      {
        text: 'Edit',
        iconName: 'pencil',
        action: () => {
            closeOpenRow();
            onPressItem(item);
        },
      },
    ];
    return <ListHiddenItems icons={icons} />;
  }, [closeOpenRow, onPressItem]);

  const onRowDidOpen = (rowKey: string, rowMap: RowMap<ShoppingList>) => {
    openRowRef.current = rowMap[rowKey];
  };

  return (
    <>
      <View>
        <SwipeListView
          data={compras}
          renderItem={renderItem}
          renderHiddenItem={renderHiddenItem}
          keyExtractor={(item) => item.id}
          rightOpenValue={-70}
          previewRowKey={'0'}
          previewOpenValue={-40}
          previewOpenDelay={10}
          onRowDidOpen={onRowDidOpen}
          disableRightSwipe={true}
        />
        {compras.length === 0 ? renderItemEmptyList() : null}
      </View>
    </>
  );
}

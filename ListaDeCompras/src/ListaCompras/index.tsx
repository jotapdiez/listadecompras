import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Lista from './Lista';
import Formulario from './Formulario';
import BarcodeScanner, { BarcodeScannerScreen, BarcodeScannerStackParamList } from '../components/BarcodeScanner';
import FormularioProducto from '../Productos/Formulario';
import {ShoppingListStackParamList, ShoppingListScreens} from './screens';
import EditShoppingListItem from './EditShoppingListItemForm';
import { ProductRootStackParamList, ProductScreens } from '../Productos/screens';
import { NavigatorHeaderStyle } from '../utils/theme';

const Stack = createStackNavigator<ShoppingListStackParamList & BarcodeScannerStackParamList & ProductRootStackParamList>();
export default function Listas() {
    // console.log('Render ListasCompraNavigator');
    return (
        <Stack.Navigator screenOptions={NavigatorHeaderStyle}>
            <Stack.Screen name={ShoppingListScreens.ShoppingListList} component={Lista} />
            <Stack.Screen name={ShoppingListScreens.ShoppingListForm} component={Formulario} />
            <Stack.Screen name={ProductScreens.ProductForm} component={FormularioProducto} />
            <Stack.Screen name={BarcodeScannerScreen.BarcodeScanner} component={BarcodeScanner} />
            <Stack.Group screenOptions={{
                presentation: 'modal',
            }}>
                <Stack.Screen
                    name={ShoppingListScreens.EditShoppingListItem}
                    component={EditShoppingListItem} />
            </Stack.Group>
        </Stack.Navigator>
    );
}

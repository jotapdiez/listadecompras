import React from 'react';
import {screen, fireEvent} from '@testing-library/react-native';
import { renderComponents, validateAndChangeInputByDisplayValue, validateAndChangeInputByTestId, validateAndChangeSelectByTestId } from '../../../__tests__/utils';
import Formulario from '../Formulario';
import store from '../../../reducers';
import {ENTITY, init, setFormItem} from '../../Slice';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import { createStackNavigator } from '@react-navigation/stack';
import { ShoppingListScreens } from '../../screens';
jest.mock('uuid', () => {return {v4: jest.fn(() => 'IDASD')};});

const mock_shopping_list = [
    {
        'id': '74f7beb5-24b9-436e-9ead-b301130ddfcf',
        'createDate': '2019-06-20T22:44:32.080+00:00',
        'updateDate': '2019-06-20T22:44:32.080+00:00',
        'title': 'Mensual',
        'productos': [
            {
                'product_id': '3b4fd0e2-7fc3-4e35-a2f3-3f2b0fda193f',
                'product_label': 'Pepsi 2.25lt',
                'price_id': 'a2b413aa-481d-436f-9e3e-121490ac1b47',
                'price_value': 300.0,
                'cantidad': 4,
                'comprados': 4,
            },
            {
                'product_id': '5b8f0380-c411-471d-bfa0-2d977ed480a0',
                'product_label': 'Frutigran 300g',
                'price_id': null,
                'price_value': 0.0,
                'cantidad': 4,
                'comprados': 2,
            },
            {
                'product_id': '35576969-78f1-509c-16af-12ad1a11be1d',
                'product_label': 'Abadie 500u',
                'price_id': null,
                'price_value': 0.0,
                'cantidad': 4,
                'comprados': 0,
            },
        ],
        'fechaEstimadaCompra': '2019-06-28T23:51:49.112+00:00',
        'importeCalculado': 350,
        'ultimoCalculo': '2019-06-13T23:51:49.112+00:00',
    },
];


beforeAll(() => {
    store.dispatch(init(mock_shopping_list));
});

test('change name and update', () => {
    store.dispatch(setFormItem(mock_shopping_list[0]));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name={ShoppingListScreens.ShoppingListForm} component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    // validateAndChangeInputByDisplayValue(mock_shopping_list[0].title, 'a1');
    validateAndChangeInputByTestId('shoplist-name', 'a1');

    fireEvent.press(screen.getByTestId('shoplist-save'));
    const stores = store.getState()[ENTITY].items;
    expect(stores).toHaveLength(1);
    const currentStore = stores[0];
    expect(currentStore.title).toBe('a1');
});

test('add new', () => {
    store.dispatch(setFormItem({pais:''}));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name={ShoppingListScreens.ShoppingListForm} component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    validateAndChangeInputByTestId('shoplist-name', 'nombre');
    // AGREGAR PRODUCTOS

    fireEvent.press(screen.getByTestId('shoplist-save'));
    const stores = store.getState()[ENTITY].items;
    expect(stores).toHaveLength(2);
    const currentStore = stores[1];
    expect(currentStore.title).toBe('nombre');
    expect(currentStore.id).toBe('IDASD');
});

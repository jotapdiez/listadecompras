import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { SwipeListView } from 'react-native-swipe-list-view';
import {
  View,
  Text,
  Input,
  Box,
  HStack,
  VStack,
  FormControl,
  Pressable,
  useToast,
  Center,
} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
// import { useIsFocused } from '@react-navigation/native';
import { Product, ShoppingList, ShoppingListItems } from '../../reducers/types';
import { setFormItem, getFormItem, addItem, updateItem } from '../Slice';
import { useAppDispatch, useAppSelector } from '../../reducers';
// import { clearBarcodeScanned, getBarcodeScanned } from '../../reducers/BarcodeSlice';
import {
  selectAll as selectAllProducts,
  setFormItem as setProductFormItem,
} from '../../Productos/Slice';
import Alert from '../../components/Alert';
// import AddProductFab from './AddProductFab';
import { default as IconButton } from '../../components/IconButton';
import { ShoppingListStackParamList, ShoppingListScreens } from '../screens';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { RowMap, SwipeRow } from 'react-native-swipe-list-view';
import { ProductScreens } from '../../Productos/screens';
import { BarcodeScannerScreen } from '../../components/BarcodeScanner';
import { HeaderButtons, ThemeColors } from '../../utils/theme';

const LOG_HIERACHY = 'ListaCompras::Formulario - ';

type Props = NativeStackScreenProps<
  ShoppingListStackParamList,
  ShoppingListScreens.ShoppingListForm
>;
export default function FormularioCompras({ navigation, route }: Props) {
  const toast = useToast();
  // const isFocused = useIsFocused();
  const productos = useAppSelector(selectAllProducts);
  const dispatch = useAppDispatch();
  const compra = useAppSelector(getFormItem);
  const [isOpenCreateProductShoppingList, setIsOpenCreateProductShoppingList] =
    useState<boolean>(false);
  const [barcode, setBarcode] = useState<string | null>(null);
  // const barcodeScanned = useAppSelector(getBarcodeScanned);
  const openRowRef = useRef<SwipeRow<ShoppingListItems> | null>(null);

  const save = useCallback(() => {
    let newCompra = Object.assign({}, compra);

    newCompra.productos ??= [];
    if (!newCompra.id) {
      dispatch(addItem(newCompra));
    } else {
      dispatch(updateItem(newCompra));
    }

    if (navigation.canGoBack()) {
      navigation.goBack();
    }
  }, [dispatch, compra, navigation]);

  const calcularTotal = useCallback(() => {
    toast.show({
      render: () => {
        return (
          <Box bg="emerald.500" px="2" py="1" rounded="sm" mb={5}>
            Implementar
          </Box>
        );
      },
    });
  }, [toast]);

  const headerRightButtons = useMemo(
    () => (
      <HStack backgroundColor={HeaderButtons.BackgroundColor}>
        <IconButton
          icon="save"
          size={30}
          color={HeaderButtons.Color}
          onPress={save}
          testID="shoplist-save"
        />
        <IconButton
          icon="calculator"
          size={30}
          color={HeaderButtons.Color}
          onPress={calcularTotal}
          testID="shoplist-calctotal"
        />
      </HStack>
    ),
    [save, calcularTotal],
  );

  useEffect(() => {
    navigation.setOptions({
      title: compra.id ? 'Editar' : 'Agregar',
      headerRight: () => headerRightButtons,
    });
  }, [navigation, headerRightButtons, compra]);

  const scanProduct = useCallback(() => {
    navigation.navigate(BarcodeScannerScreen.BarcodeScanner, {
      backTo: ShoppingListScreens.ShoppingListForm,
    });
  }, [navigation]);

  const update = useCallback(
    (item: ShoppingList) => {
      dispatch(setFormItem(Object.assign({}, compra, item)));
    },
    [dispatch, compra],
  );

  const productoSelected = useCallback(
    (producto: Product) => {
      if (!compra.productos) {
        compra.productos = [];
      }
      let shoplistProducts = [...compra.productos];
      shoplistProducts.push({
        cantidad: 1,
        comprados: 0,
        product_id: producto.id,
        product_label: `${producto.nombre} ${producto.cantidadUnidad}${producto.unidad}`,
      } as ShoppingListItems);
      update(Object.assign({}, compra, { productos: shoplistProducts }));
    },
    [compra, update],
  );

  useEffect(() => {
    console.log(LOG_HIERACHY, 'route.params?.barcodeScanned =', route.params?.barcodeScanned);
    // console.log(LOG_HIERACHY,'route.params?.product =',route.params?.product);
    if (route.params?.barcodeScanned) {
      let prod = productos.find((p: Product) => p.barcode === route.params?.barcodeScanned);
      if (prod?.id) {
        productoSelected(prod);
        navigation.setParams({ barcodeScanned: undefined });
      } else if (!isOpenCreateProductShoppingList) {
        setBarcode(route.params?.barcodeScanned);
        setIsOpenCreateProductShoppingList(true);
      }
    }

    // if (route.params?.product) {
    //   const prod = productos.find((p: Product) => p.barcode === barcode);
    //   if (prod) {
    //     productoSelected(prod);
    //     setBarcode(null);
    //     navigation.setParams({
    //       product: undefined,
    //     });
    //   }
    // }

    // if (route.params?.product) {
    //   console.log(
    //     LOG_HIERACHY,
    //     'route.params?.product =',
    //     route.params?.product,
    //   );
    //   productoSelected(route.params?.product);
    // }
  }, [route, productos, productoSelected]);

  // useEffect(() => {
  //   if (barcode === null) {
  //     return;
  //   }

  //   const prod = productos.find((p: Product) => p.barcode === barcode);
  //   if (!prod) {
  //     return;
  //   }

  //   productoSelected(prod);
  //   return () => {
  //     setBarcode(null);
  //   };
  // }, [productos, productoSelected, barcode]);

  const okPromptAddProduct = useCallback(() => {
    setIsOpenCreateProductShoppingList(false);
    const newProduct = { barcode } as Product;
    dispatch(setProductFormItem(Object.assign({} as Product, newProduct)));
    navigation.navigate(ProductScreens.ProductForm, {
      backTo: ShoppingListScreens.ShoppingListForm,
    });
  }, [barcode, dispatch, navigation]);

  const agregarProducto = useCallback(() => {
    toast.show({
      render: () => {
        return (
          <Box bg="emerald.500" px="2" py="1" rounded="sm" mb={5}>
            Implementar
          </Box>
        );
      },
    });
  }, [toast]);

  const markOne = useCallback(
    (product_id: string) => {
      const shoppingListItemIndex = compra.productos.findIndex(
        (item: ShoppingListItems) => item.product_id === product_id,
      );

      if (
        compra.productos[shoppingListItemIndex].comprados ===
        compra.productos[shoppingListItemIndex].cantidad
      ) {
        compra.productos[shoppingListItemIndex].cantidad++;
      }
      compra.productos[shoppingListItemIndex].comprados++;

      update(Object.assign({}, compra));
    },
    [compra, update],
  );

  const renderNewItemProductList = useCallback(() => {
    return (
      <HStack rounded="8" space={[2, 3]} justifyContent="space-evenly" p="2" px={4}>
        <VStack>
          <Text>Escanear producto</Text>
          <IconButton
            icon="barcode"
            size={30}
            color="black"
            onPress={scanProduct}
            testID="shoplist-form-prod-add"
          />
        </VStack>
        <VStack>
          <Text>Agregar producto</Text>
          <IconButton
            icon="plus"
            size={30}
            color="black"
            onPress={agregarProducto}
            testID="shoplist-form-prod-scan"
          />
        </VStack>
      </HStack>
    );
  }, [scanProduct, agregarProducto]);

  const renderItemProductList = useCallback(
    ({ item }) => {
      const { product_label, product_id, cantidad, comprados } = item;
      return (
        <Box py="1">
          <HStack
            rounded="8"
            borderWidth="1"
            borderColor="coolGray.300"
            space={[2, 3]}
            justifyContent="space-between"
            shadow="3"
            bg="coolGray.100"
            p="2"
            px={4}
          >
            <VStack>
              <Text bold color="black">
                {product_label}
              </Text>
              <Text color="coolGray.500" fontSize="xs">
                {comprados}/{cantidad}
              </Text>
            </VStack>
            <HStack justifyContent="space-between">
              <IconButton
                icon="cart-arrow-down"
                size={20}
                color="black"
                onPress={() => markOne(product_id)}
              />
              {/* <IconButton
                icon="angle-double-up"
                size={20}
                color="black"
                onPress={() => {} markAll(product_id)}
              /> */}
            </HStack>
          </HStack>
        </Box>
      );
    },
    [markOne],
  );

  const renderItemEmptyProductList = useCallback(() => {
    return (
      <Center>
        <Text>No se agregaron productos</Text>
      </Center>
    );
  }, []);

  const modificar = useCallback(() => {
    closeOpenRow();
    if (!openRowRef.current?.props.item) {
      return;
    }

    navigation.navigate(ShoppingListScreens.EditShoppingListItem, {
      data: openRowRef.current?.props.item,
    });
  }, [navigation]);

  const renderHiddenItem = useCallback(
    () => (
      <HStack flex="1" pl="2">
        <Pressable
          w="70"
          ml="auto"
          my="1.5"
          bg="grey"
          rounded="8"
          justifyContent="center"
          _pressed={{
            opacity: 0.5,
          }}
          onPress={modificar}
        >
          <VStack alignItems="center">
            <Icon name="pencil" color="white" />
            <Text fontWeight="medium" color="white">
              Modificar
            </Text>
          </VStack>
        </Pressable>
      </HStack>
    ),
    [modificar],
  );

  const closeOpenRow = () => {
    if (openRowRef.current && openRowRef.current.closeRow) {
      openRowRef.current.closeRow();
    }
  };

  const onRowOpen = (rowKey: string, rowMap: RowMap<ShoppingListItems>) => {
    openRowRef.current = rowMap[rowKey];
  };

  console.log('Render FormularioCompras');
  return (
    <>
      <Alert
        title="Producto inexistente"
        isOpen={isOpenCreateProductShoppingList}
        text="El producto escaneado no existe. Desea crearlo y agregarlo?"
        onCancel={() => {
          setIsOpenCreateProductShoppingList(false);
        }}
        onOk={okPromptAddProduct}
      />
      <Box px="2" py="1">
        <FormControl>
          <VStack h={'100%'} mt={2}>
            {/* <HStack space={3} alignItems={'center'}>
              <HStack mx="4" space={3} flexGrow={1} alignItems={'center'}>
                <Text>{moment(compra.fechaEstimadaCompra).format('L')}</Text>
                <IconButton icon="calendar" onPress={changeBuyDate} />
              </HStack>
            </HStack> */}
            <HStack space={1} px={2}>
              <FormControl.Label>Nombre</FormControl.Label>
              <Input
                flexGrow={1}
                testID="shoplist-name"
                placeholder="Nombre..."
                value={compra.title}
                onChangeText={(title) => update(Object.assign({}, compra, { title }))}
              />
            </HStack>
            <Box flex="1" my={2} mb={7}>
              <Box>{renderNewItemProductList()}</Box>
              <Box>{compra.productos?.length === 0 ? renderItemEmptyProductList() : null}</Box>
              <Box flex={1}>
                <SwipeListView
                  useFlatList={true}
                  data={compra.productos}
                  renderItem={renderItemProductList}
                  renderHiddenItem={renderHiddenItem}
                  onRowOpen={onRowOpen}
                  rightOpenValue={-70}
                  previewRowKey={'0'}
                  previewOpenValue={-40}
                  previewOpenDelay={0}
                  previewRepeatDelay={0}
                  disableRightSwipe={true}
                  keyExtractor={(item, index) => item.product_id + index}
                />
              </Box>
              <Box>
                <HStack
                  space={3}
                  width="100%"
                  py={2}
                  justifyContent={'center'}
                  alignItems={'center'}
                >
                  <Text>Productos: {compra.productos?.length ?? 0}</Text>
                  <Text>Importe total: ${compra.importeCalculado}</Text>
                </HStack>
              </Box>
            </Box>
          </VStack>
        </FormControl>
      </Box>
    </>
  );
}

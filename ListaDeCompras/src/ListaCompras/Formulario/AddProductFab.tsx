import React from 'react';
import { Fab } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

interface AddProductFabProps {
  scanProduct: () => void;
  agregarProducto: () => void;
}

const AddProductFab = React.memo(
  ({scanProduct, agregarProducto}: AddProductFabProps) => {
    // console.log('Render AddProductFab');
    return (
      <>
        <Fab
          renderInPortal={false}
          shadow={2}
          size="xs"
          placement="bottom-right"
          icon={<Icon name="barcode" color="white" size={20} />}
          bottom={100}
          onPress={scanProduct}
        />
        <Fab
          renderInPortal={false}
          size="sm"
          placement="bottom-right"
          icon={<Icon name="plus" color="white" size={20} />}
          bottom={160}
          onPress={agregarProducto}
        />
      </>
    );
  }
);

export default AddProductFab;

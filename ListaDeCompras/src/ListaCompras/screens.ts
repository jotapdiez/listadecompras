import { ProductScreens } from '../Productos/screens';
import { Product, ShoppingListItems } from '../reducers/types';

export enum ShoppingListScreens {
    ShoppingListList = 'List',
    ShoppingListForm = 'ShoppingListForm',
    EditShoppingListItem = 'EditShoppingListItem'
}

export type ShoppingListStackParamList = {
    [ShoppingListScreens.ShoppingListList]: undefined;
    [ShoppingListScreens.ShoppingListForm]: { barcodeScanned:string|undefined, product:Product|undefined } | undefined;
    [ProductScreens.ProductForm]: { backTo:string } | undefined;
    Barcodescanner: { backTo:string } | undefined;
    Options: undefined;
    [ShoppingListScreens.EditShoppingListItem]: { data:ShoppingListItems } | undefined;
};

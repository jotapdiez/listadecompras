import React, { useCallback, useEffect, useState } from 'react';
import { Box, FormControl, HStack, Input, Stack, Text, VStack } from 'native-base';
import { ShoppingListItems } from '../../reducers/types';
import { default as IconButton } from '../../components/IconButton';
import { useAppDispatch, useAppSelector } from '../../reducers';
import { getFormItem, setFormItem } from '../Slice';
import { useRoute } from '@react-navigation/native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { ShoppingListScreens, ShoppingListStackParamList } from '../screens';
import { HeaderButtons, ThemeColors } from '../../utils/theme';

type Props = NativeStackScreenProps<
  ShoppingListStackParamList,
  ShoppingListScreens.EditShoppingListItem
>;
const EditShoppingListItem = ({ navigation }: Props) => {
  const dispatch = useAppDispatch();
  const compra = useAppSelector(getFormItem);
  // const navigationx = useNavigation();
  const route = useRoute<any>();
  const [item, setItem] = useState<ShoppingListItems>(route.params?.data);

  const doSave = useCallback(() => {
    // const prodItem = compra.productos.find(
    //     (p: ShoppingListItems) => p.product_id === item.product_id,
    // );
    // if (!prodItem) {
    //     console.log("SAVE ERROR !prodItem - item.product_id=", item.product_id);
    //     //TODO: useToast con el error
    //     return;
    // }

    // prodItem.cantidad = item.cantidad;
    // prodItem.comprados = item.comprados;
    const productos = compra.productos.map((p: ShoppingListItems) => {
      // console.log(`${p.product_id}  === ${item.product_id}`);
      const newP = { ...p };
      if (newP.product_id === item.product_id) {
        newP.cantidad = item.cantidad;
        newP.comprados = item.comprados;
      }

      return newP;
    });
    dispatch(setFormItem(Object.assign({}, compra, { productos })));

    if (navigation.canGoBack()) {
      navigation.goBack();
    }
  }, [navigation, dispatch, compra, item]);

  useEffect(() => {
    navigation.setOptions({
      title: 'Modificar producto en Lista de compra',
      // eslint-disable-next-line react/no-unstable-nested-components
      headerRight: () => (
        <HStack backgroundColor={HeaderButtons.BackgroundColor}>
          <IconButton
            icon="save"
            size={30}
            color={HeaderButtons.Color}
            onPress={doSave}
            testID="shopItem-save"
          />
        </HStack>
      ),
    });
  }, [navigation, doSave]);

  const updateItem = (updated) => {
    setItem(Object.assign({}, item, updated));
  };
  const increaseComprados = () => {
    updateItem({
      comprados: item.comprados + 1,
    });
  };
  const decreaseComprados = () => {
    updateItem({
      comprados: item.comprados - 1,
    });
  };
  const setComprados = (comprados: string) => {
    updateItem({ comprados });
  };
  const increaseCantidad = () => {
    updateItem({
      cantidad: item.cantidad + 1,
    });
  };
  const decreaseCantidad = () => {
    updateItem({
      cantidad: item.cantidad - 1,
    });
  };
  const setCantidad = (cantidad: string) => {
    updateItem({ cantidad });
  };

  // console.log('Render EditShoppingListItem item', item);
  // console.log('Render EditShoppingListItem');
  return (
    <Box w="100%" px={2} my={0} justifyContent="center">
      <Text>
        Modificar{' '}
        <Text italic bold>
          {item?.product_label}
        </Text>
      </Text>
      <VStack space={1}>
        <Stack>
          <FormControl.Label>Cantidad</FormControl.Label>
          <HStack space={1}>
            <Input
              flexGrow={1}
              testID="shopItem-cantidad"
              keyboardType="number-pad"
              value={item?.cantidad.toString()}
              onChangeText={setCantidad}
            />
            <IconButton icon="plus" onPress={increaseCantidad} />
            <IconButton icon="minus" onPress={decreaseCantidad} />
          </HStack>
        </Stack>
        <Stack>
          <FormControl.Label>Comprados</FormControl.Label>
          <HStack space={1}>
            <Input
              flexGrow={1}
              testID="shopItem-comprados"
              keyboardType="number-pad"
              value={item?.comprados.toString()}
              onChangeText={setComprados}
            />
            <IconButton icon="plus" onPress={increaseComprados} />
            <IconButton icon="minus" onPress={decreaseComprados} />
          </HStack>
        </Stack>
      </VStack>
    </Box>
  );
};

// export default EditShoppingListItem;
export default React.memo(EditShoppingListItem, (prevProps, nextProps) => {
  // Are props equals?
  // console.log(
  //     'Are props equals?',
  //     '\nprev=', prevProps.data,
  //     '\nnext=', nextProps.data,
  // );
  // return prevProps.data.product_id === nextProps.data.product_id &&
  //     prevProps.data.comprados === nextProps.data.comprados &&
  //     prevProps.data.cantidad === nextProps.data.cantidad;
  return prevProps.route?.params?.data.product_id === nextProps.route?.params?.data.product_id;
});

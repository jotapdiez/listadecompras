import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Formulario from './Formulario';
import Lista from './Lista';
import {ShoppingListStackParams, StoreScreens} from './screens';
import { NavigatorHeaderStyle } from '../utils/theme';

const Stack = createStackNavigator<ShoppingListStackParams>();
export default function Tiendas() {
    // console.log('Render TiendasNavigator');
    return (
        <Stack.Navigator screenOptions={NavigatorHeaderStyle}>
            <Stack.Screen name={StoreScreens.List} component={Lista} />
            <Stack.Group
            screenOptions={{
                presentation: 'modal',
            }}>
                <Stack.Screen name={StoreScreens.Form} component={Formulario} />
            </Stack.Group>
        </Stack.Navigator>
    );
}

import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { View, Text, IconButton, Box, HStack } from 'native-base';
import { useAppDispatch, useAppSelector } from '../../reducers';
import { selectAll, setFormItem } from '../Slice';
import { Store } from '../../reducers/types';
import { RowMap, SwipeListView, SwipeRow } from 'react-native-swipe-list-view';
import {ShoppingListStackParams, StoreScreens} from '../screens';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import { getOptions } from '../../Options/Slice';
import ListItem from '../../components/ListItem';
import ListHiddenItems from '../../components/ListHiddenItems';
import { HeaderButtons, ThemeColors } from '../../utils/theme';

type Props = NativeStackScreenProps<ShoppingListStackParams, StoreScreens.List>;
export default function ListaTiendas ({navigation}:Props) {
  const dispatch = useAppDispatch();
  const tiendas = useAppSelector(selectAll);
  const openRowRef = useRef<SwipeRow<Store> | null>(null);
  const options = useAppSelector(getOptions);

  const mostrarFormulario = useCallback(() => {
    navigation.navigate(StoreScreens.Form);
  }, [navigation]);

  const doAdd = useCallback(() => {
    dispatch(
      setFormItem({
        pais: options.defaultCountry,
        ciudad: options.defaultCity,
      } as Store),
    );
    mostrarFormulario();
  }, [dispatch, mostrarFormulario, options]);

  const headerRightButtons = useMemo(
    () => (
      <HStack backgroundColor={HeaderButtons.BackgroundColor}>
        <IconButton
          icon={<Icon name="plus" color={HeaderButtons.Color} size={30} />}
          onPress={doAdd}
        />
      </HStack>
    ),
    [doAdd],
  );

  useEffect(() => {
    navigation.setOptions({
      title: '',
      headerRight: () => headerRightButtons,
    });
  }, [headerRightButtons, navigation]);

  const edit = useCallback((store: Store) => {
    dispatch(setFormItem(store));
    mostrarFormulario();
  }, [dispatch, mostrarFormulario]);

  const renderItem = useCallback(({item}) => {
    return (
      <ListItem
        title={item.nombre}
        subtitle={`${item.direccion}, ${item.ciudad}`}
      />
    );
  }, []);

  const renderItemEmptyList = useCallback(() => {
    return (<Box pl={['1', '4']} pr={['1', '5']} py="1">
        <Text>No hay tiendas</Text>
    </Box>);
  }, []);

  const closeOpenRow = useCallback(() => {
    if (openRowRef.current && openRowRef.current.closeRow) {
      openRowRef.current.closeRow();
    }
  }, []);

  const renderHiddenItem = useCallback(({item}) => {
    const icons = [
      {
        text: 'Edit',
        iconName: 'pencil',
        action: () => {
          closeOpenRow();
          edit(item);
        },
      },
    ];
    return <ListHiddenItems icons={icons} />;
  }, [edit, closeOpenRow]);

  const onRowDidOpen = (rowKey: string, rowMap: RowMap<Store>) => {
    openRowRef.current = rowMap[rowKey];
  };

  // console.log('Render ListaTiendas');
  if (tiendas.length === 0) {
    return renderItemEmptyList();
  }

  return (
    <>
      <View>
        <SwipeListView
          data={tiendas}
          renderItem={renderItem}
          renderHiddenItem={renderHiddenItem}
          rightOpenValue={-70}
          previewRowKey={'0'}
          previewOpenValue={-40}
          previewOpenDelay={10}
          onRowDidOpen={onRowDidOpen}
          disableRightSwipe={true}
          keyExtractor={(item) => item.id}
        />
      </View>
    </>
  );
}

export enum StoreScreens {
    List = 'List',
    Form = 'Form',
}
export type ShoppingListStackParams = {
    [StoreScreens.List]: undefined;
    [StoreScreens.Form]: undefined;
    Options: undefined;
};

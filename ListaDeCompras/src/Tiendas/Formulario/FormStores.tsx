import React, { useCallback, useEffect, useMemo } from 'react';
import { FormControl, Stack, Input, Select, IconButton, HStack, Toast } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useAppDispatch, useAppSelector } from '../../reducers';
import { addItem, getFormItem, setFormItem, updateItem } from '../Slice';
import { Store } from '../../reducers/types';
import { ShoppingListStackParams, StoreScreens } from '../screens';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { HeaderButtons, ThemeColors } from '../../utils/theme';

type Props = NativeStackScreenProps<ShoppingListStackParams, StoreScreens.Form>;
export default function FormularioTienda({ navigation }: Props) {
  const dispatch = useAppDispatch();
  const tienda = useAppSelector(getFormItem);

  const save = useCallback(() => {
    if (!tienda.nombre) {
      Toast.show({ title: 'Error', description: 'El nombre es requerido' });
      return;
    }

    if (!tienda.direccion) {
      Toast.show({ title: 'Error', description: 'La direccion es requerida' });
      return;
    }

    if (!tienda.ciudad) {
      Toast.show({ title: 'Error', description: 'La ciudad es requerida' });
      return;
    }

    if (!tienda.pais || tienda.pais === '') {
      Toast.show({ title: 'Error', description: 'El pais es requerido' });
      return;
    }

    if (!tienda.id) {
      dispatch(addItem(Object.assign({}, tienda)));
    } else {
      dispatch(updateItem(Object.assign({}, tienda)));
    }

    if (navigation.canGoBack()) {
      navigation.goBack();
    }
  }, [dispatch, tienda, navigation]);

  const headerRightButtons = useMemo(
    () => (
      <HStack backgroundColor={HeaderButtons.BackgroundColor}>
        <IconButton
          icon={<Icon name="save" size={30} color={HeaderButtons.Color} />}
          onPress={save}
          testID="store-save"
        />
      </HStack>
    ),
    [save],
  );

  useEffect(
    () =>
      navigation.setOptions({
        title: tienda.id ? 'Actualizar tienda' : 'Agregar tienda',
        headerRight: () => headerRightButtons,
      }),
    [headerRightButtons, navigation, save, tienda],
  );

  const update_tienda = useCallback(
    (obj: Store) => {
      let state = Object.assign({}, tienda, obj);
      dispatch(setFormItem(state));
    },
    [dispatch, tienda],
  );

  // console.log('Render FormularioTienda');
  return (
    <FormControl>
      <Stack mt="2" px="2">
        <FormControl.Label>Nombre</FormControl.Label>
        <Input
          testID="Nombre"
          value={tienda.nombre}
          onChangeText={(nombre) => update_tienda(Object.assign({}, tienda, { nombre }))}
        />
      </Stack>
      <Stack mt="2" px="2">
        <FormControl.Label>Direccion</FormControl.Label>
        <Input
          testID="Direccion"
          value={tienda.direccion}
          onChangeText={(direccion) => update_tienda(Object.assign({}, tienda, { direccion }))}
        />
      </Stack>
      <Stack mt="2" px="2">
        <FormControl.Label>Pais</FormControl.Label>
        <Select
          testID="country-select"
          variant="outline"
          selectedValue={tienda?.pais.toLowerCase() || ''}
          onValueChange={(pais) => update_tienda(Object.assign({}, tienda, { pais }))}
        >
          <Select.Item label="Argentina" value="argentina" />
          <Select.Item label="Colombia" value="colombia" />
          <Select.Item label="Estados Unidos" value="eeuu" />
        </Select>
      </Stack>
      <Stack mt="2" px="2">
        <FormControl.Label>Ciudad</FormControl.Label>
        <Input
          testID="Ciudad"
          value={tienda.ciudad}
          onChangeText={(ciudad) => update_tienda(Object.assign({}, tienda, { ciudad }))}
        />
      </Stack>
    </FormControl>
  );
}

import React from 'react';
import {screen, fireEvent} from '@testing-library/react-native';
import Formulario from '../FormStores';
import { renderComponents, validateAndChangeInputByDisplayValue, validateAndChangeInputByTestId, validateAndChangeSelectByTestId } from '../../../__tests__/utils';
import store from '../../../reducers';
import {ENTITY, init, setFormItem} from '../../../Tiendas/Slice';
import {init as initOptions} from '../../../Options/Slice';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import { createStackNavigator } from '@react-navigation/stack';
jest.mock('uuid', () => {
    return {v4: jest.fn(() => 'IDASD')};
});

const mock_stores = [
    {
        id: '3b4fd0e2-7fc3-4e35-a2f3-3f2b0fda193f',
        createDate: '2020-09-22T03:16:08.287+00:00',
        updateDate: null,
        pais: 'Argentina',
        ciudad: 'San Martin',
        nombre: 'Carrefour',
        direccion: 'Av. San Martín 421',
    },
];


beforeAll(() => {
    store.dispatch(initOptions({defaultCountry:'', defaultCity:'', defaultStore:''} as Options));
    store.dispatch(init(mock_stores));
});

test('change name and update', () => {
    store.dispatch(setFormItem(mock_stores[0]));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    validateAndChangeInputByDisplayValue(mock_stores[0].nombre, 'a1');

    fireEvent.press(screen.getByTestId('store-save'));
    const stores = store.getState()[ENTITY].items;
    expect(stores).toHaveLength(1);
    const currentStore = stores[0];
    expect(currentStore.nombre).toBe('a1');
});

test('change country and update', async () => {
    store.dispatch(setFormItem(mock_stores[0]));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    validateAndChangeSelectByTestId('country-select', 'colombia');

    fireEvent.press(screen.getByTestId('store-save'));
    const stores = store.getState()[ENTITY].items;
    expect(stores).toHaveLength(1);
    const currentStore = stores[0];
    expect(currentStore.pais).toBe('colombia');
});

test('change city and update', () => {
    store.dispatch(setFormItem(mock_stores[0]));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    validateAndChangeInputByDisplayValue(mock_stores[0].ciudad, 'ciudad');

    fireEvent.press(screen.getByTestId('store-save'));
    const stores = store.getState()[ENTITY].items;
    expect(stores).toHaveLength(1);
    const currentStore = stores[0];
    expect(currentStore.ciudad).toBe('ciudad');
});

test('change address and update', () => {
    store.dispatch(setFormItem(mock_stores[0]));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    validateAndChangeInputByDisplayValue(mock_stores[0].direccion, 'direccion');

    fireEvent.press(screen.getByTestId('store-save'));
    const stores = store.getState()[ENTITY].items;
    expect(stores).toHaveLength(1);
    const currentStore = stores[0];
    expect(currentStore.direccion).toBe('direccion');
});

test('add new invalid name', () => {
    store.dispatch(setFormItem({pais:''}));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    fireEvent.press(screen.getByTestId('store-save'));
    const stores = store.getState()[ENTITY].items;
    expect(stores).toHaveLength(1);
});

test('add new invalid address', () => {
    store.dispatch(setFormItem({pais:''}));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    validateAndChangeInputByTestId('Nombre', 'nombre');

    // screen.debug();
    fireEvent.press(screen.getByTestId('store-save'));
    const stores = store.getState()[ENTITY].items;
    expect(stores).toHaveLength(1);
});

test('add new invalid country', () => {
    store.dispatch(setFormItem({pais:''}));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    validateAndChangeInputByTestId('Nombre', 'nombre');
    validateAndChangeInputByTestId('Direccion', 'direccion');

    // screen.debug();
    fireEvent.press(screen.getByTestId('store-save'));
    const stores = store.getState()[ENTITY].items;
    expect(stores).toHaveLength(1);
});

test('add new invalid city', () => {
    store.dispatch(setFormItem({pais:''}));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    validateAndChangeInputByTestId('Nombre', 'nombre');
    validateAndChangeInputByTestId('Direccion', 'direccion');
    validateAndChangeSelectByTestId('country-select', 'pais');

    // screen.debug();
    fireEvent.press(screen.getByTestId('store-save'));
    const stores = store.getState()[ENTITY].items;
    expect(stores).toHaveLength(1);
});

test('add new', () => {
    store.dispatch(setFormItem({pais:''}));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    validateAndChangeInputByTestId('Nombre', 'nombre');
    validateAndChangeSelectByTestId('country-select', 'pais');
    validateAndChangeInputByTestId('Ciudad', 'ciudad');
    validateAndChangeInputByTestId('Direccion', 'direccion');

    fireEvent.press(screen.getByTestId('store-save'));
    const stores = store.getState()[ENTITY].items;
    expect(stores).toHaveLength(2);
    const currentStore = stores[1];
    expect(currentStore.nombre).toBe('nombre');
    expect(currentStore.pais).toBe('pais');
    expect(currentStore.ciudad).toBe('ciudad');
    expect(currentStore.direccion).toBe('direccion');
    expect(currentStore.id).toBe('IDASD');
});

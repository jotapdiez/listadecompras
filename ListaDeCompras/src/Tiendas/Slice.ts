import 'react-native-get-random-values';
import { v4 as uuidv4 } from 'uuid';
import { createSlice  } from '@reduxjs/toolkit';
import { AppState } from '../reducers';
import { Store } from '../reducers/types';

const initialState = {
    items: [] as Store[],
    formItem: null,
    status: 'idle',
    error: null,
};

export const ENTITY = 'store';

const updateObject = (object: Store, updatedObject: Store) => {
    object.id = updatedObject.id;
    object.pais = updatedObject.pais;
    object.ciudad = updatedObject.ciudad;
    object.nombre = updatedObject.nombre;
    object.direccion = updatedObject.direccion;
    object.createDate = updatedObject.createDate;
    object.updateDate = updatedObject.updateDate;
};

export const StoreSlice = createSlice({
    name: ENTITY,
    initialState: initialState,
    reducers: {
        init: (state, action) => {
            state.items = action.payload;
        },
        setFormItem: (state, action) => {
            state.formItem = action.payload;
        },
        updateFormItem: (state, action) => {
            state.formItem = action.payload;
        },
        addItem: (state, action) => {
            const newItem = Object.assign({}, action.payload, { id: uuidv4() });
            state.items.push(newItem);
        },
        updateItem: (state, action) => {
            state.status = 'done';
            const {id} = action.payload;
            const existingItem = state.items.find((item:Store) => item.id === id);
            if (existingItem) {
                updateObject(existingItem, action.payload);
            }
        },
        removeItem: (state, action) => {
            state.status = 'done';
            const {id} = action.payload;
            const existingItem = state.items.find((item:Store) => item.id === id) || {} as Store;
            const existingItemIndex = state.items.indexOf(existingItem);
            if (existingItemIndex > -1) {
                const deletedItems = state.items.splice(existingItemIndex, 1);
                console.log('StoreSlice::removeItem=', deletedItems);
            }
        },
    },
});

export const { init, setFormItem, updateFormItem, addItem, updateItem, removeItem } = StoreSlice.actions;

export const getStatus = (state: AppState):string => state[ENTITY].status;
export const getFormItem = (state: AppState):Store => state[ENTITY].formItem;
export const selectAll = (state: AppState):Store[] => state[ENTITY].items;
export const selectById = (state: AppState, itemId: string):Store => state[ENTITY].items.find((item: Store) => item.id === itemId);

export default StoreSlice.reducer;

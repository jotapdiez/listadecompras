import {render, screen, fireEvent} from '@testing-library/react-native';
import {NavigationContainer} from '@react-navigation/native';
import {NativeBaseProvider} from 'native-base';
import React from 'react';
import { Provider } from 'react-redux';

export function renderComponents({store, ui}) {
    const metrics = {
        frame: {x: 0, y: 0, width: 0, height: 0},
        insets: {top: 0, left: 0, right: 0, bottom: 0},
    };
    return render(<Provider store={store}>
            <NativeBaseProvider initialWindowMetrics={metrics}>
                <NavigationContainer>
                    {ui}
                </NavigationContainer>
            </NativeBaseProvider>
        </Provider>);

}

export function validateAndChangeInputByDisplayValue(value: string, newValue: any) {
    const input = screen.getByDisplayValue(value);
    expect(input).toBeTruthy();

    fireEvent.changeText(input, newValue);
}

export function validateAndChangeSelectByTestId(value: string, newValue: any) {
    const select = screen.getByTestId(value);
    expect(select).toBeTruthy();

    fireEvent(select, 'onValueChange', newValue);
}

export function validateAndChangeInputByTestId(value: string, newValue: any) {
    const input = screen.getByTestId(value);
    expect(input).toBeTruthy();

    fireEvent.changeText(input, newValue);
}

import { Product } from '../reducers/types';
import { PriceScreens } from './Prices/screens';

export enum ProductScreens {
    ProductList = 'ProductList',
    PriceForm = 'FormularioPrecio',
    ProductForm = 'ProductForm',
    Barcodescanner = 'Barcodescanner',
    Options = 'Options',
    PricesList = 'PricesList',
    ListaPreciosPorTienda = 'ListaPreciosPorTienda',
}

export type ProductRootStackParamList = {
    [ProductScreens.ProductList]: { barcodeScanned:string|undefined } | undefined;
    [PriceScreens.PriceForm]: undefined;
    [ProductScreens.ProductForm]: { onSave: (product:Product) => void, backTo:string } | undefined;
    BarcodeScanner: { backTo:string } | undefined;
    Options: undefined;
    [ProductScreens.ListaPreciosPorTienda]: undefined;
    [ProductScreens.PriceForm]: undefined;
};

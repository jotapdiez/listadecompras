import React from 'react';
import {screen, fireEvent} from '@testing-library/react-native';
import Formulario from '../FormularioProducto';
import { renderComponents, validateAndChangeInputByDisplayValue, validateAndChangeInputByTestId, validateAndChangeSelectByTestId } from '../../../__tests__/utils';
import store from '../../../reducers';
import {ENTITY, init, setFormItem} from '../../Slice';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import { createStackNavigator } from '@react-navigation/stack';
jest.mock('uuid', () => {
  return {
    v4: jest.fn(() => 'IDASD'),
  };
});

const products = [
    {
        id: '3b4fd0e2-7fc3-4e35-a2f3-3f2b0fda193f',
        createDate: '2020-09-22T03:16:08.287+00:00',
        updateDate: null,
        nombre: 'Gaseosa Pepsi',
        cantidadUnidad: 2.25,
        unidad: 'Lt',
        barcode: 7791813423119,
        categoria: 'Bebidas',
        subcategoria: 'Gaseosa',
        marca: 'Pepsi',
        origen: 'preciosmaximos',
        observaciones: 'obs...',
    },
];


beforeAll(() => {
    store.dispatch(init(products));
});

test('change name and update', () => {
    store.dispatch(setFormItem(products[0]));
    const Stack = createStackNavigator();
    // console.log('store.getState()', store.getState()[ProductEntity].formItem);
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    validateAndChangeInputByDisplayValue('Gaseosa Pepsi', 'a1');

    fireEvent.press(screen.getByTestId('producto-save'));
    const productos = store.getState()[ENTITY].items;
    expect(productos).toHaveLength(1);
    const producto = productos[0];
    // console.log(producto);
    expect(producto.nombre).toBe('a1');
    // expect(producto.updateDate).not.toBeNull();
});

test('change cantidadUnidad and update', () => {
    store.dispatch(setFormItem(products[0]));
    const Stack = createStackNavigator();
    // console.log('store.getState()', store.getState()[ProductEntity].formItem);
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    validateAndChangeInputByDisplayValue('2.25', '1');

    fireEvent.press(screen.getByTestId('producto-save'));
    const productos = store.getState()[ENTITY].items;
    expect(productos).toHaveLength(1);
    const producto = productos[0];
    // console.log(producto);
    expect(producto.cantidadUnidad).toBe('1');
    // expect(producto.updateDate).not.toBeNull();
});

test('change barcode and update', () => {
    store.dispatch(setFormItem(products[0]));
    const Stack = createStackNavigator();
    // console.log('store.getState()', store.getState()[ProductEntity].formItem);
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    validateAndChangeInputByDisplayValue('7791813423119', 11111111111);

    fireEvent.press(screen.getByTestId('producto-save'));
    const productos = store.getState()[ENTITY].items;
    expect(productos).toHaveLength(1);
    const producto = productos[0];
    // console.log(producto);
    expect(producto.barcode).toBe(11111111111);
    // expect(producto.updateDate).not.toBeNull();
});

test('change observaciones and update', () => {
    store.dispatch(setFormItem(products[0]));
    const Stack = createStackNavigator();
    // console.log('store.getState()', store.getState()[ProductEntity].formItem);
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    validateAndChangeInputByDisplayValue('obs...', 'observaciones');

    fireEvent.press(screen.getByTestId('producto-save'));
    const productos = store.getState()[ENTITY].items;
    expect(productos).toHaveLength(1);
    const producto = productos[0];
    // console.log(producto);
    expect(producto.observaciones).toBe('observaciones');
    // expect(producto.updateDate).not.toBeNull();
});

test('change unidad and update', async () => {
    store.dispatch(setFormItem(products[0]));
  // https://testing-library.com/docs/ecosystem-react-select-event/
    const Stack = createStackNavigator();
    // console.log('store.getState()', store.getState()[ProductEntity].formItem);
    renderComponents({
        store,
        ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
            <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>
        ),
    });

    validateAndChangeSelectByTestId('prod-unidad', 'lt');

    fireEvent.press(screen.getByTestId('producto-save'));
    const productos = store.getState()[ENTITY].items;
    expect(productos).toHaveLength(1);
    const producto = productos[0];
    // console.log(producto);
    expect(producto.unidad.toLocaleLowerCase()).toBe('lt');
    // expect(producto.updateDate).not.toBeNull();
});

test('add new invalid name', () => {
    store.dispatch(setFormItem({barcode:''}));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    fireEvent.press(screen.getByTestId('producto-save'));
    const stores = store.getState()[ENTITY].items;
    expect(stores).toHaveLength(1);
});

test('add new', () => {
    store.dispatch(setFormItem({barcode:''}));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    validateAndChangeInputByTestId('prod-name', 'nombre');
    validateAndChangeInputByTestId('prod-cantidad', '3');
    validateAndChangeSelectByTestId('prod-unidad', 'unidad');
    validateAndChangeInputByTestId('prod-barcode', 'barcode');
    validateAndChangeInputByTestId('prod-observaciones', 'observaciones');

    fireEvent.press(screen.getByTestId('producto-save'));
    const stores = store.getState()[ENTITY].items;
    expect(stores).toHaveLength(2);
    const currentStore = stores[1];
    expect(currentStore.nombre).toBe('nombre');
    expect(currentStore.cantidadUnidad).toBe('3');
    expect(currentStore.unidad).toBe('unidad');
    expect(currentStore.barcode).toBe('barcode');
    expect(currentStore.observaciones).toBe('observaciones');
    expect(currentStore.id).toBe('IDASD');
});

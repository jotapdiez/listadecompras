import React, { useCallback, useEffect } from 'react';
import { Alert } from 'react-native';
import { FormControl, Stack, Input, Select, IconButton, HStack, Toast } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useAppDispatch, useAppSelector} from '../../reducers';
import { addItem, getFormItem, setFormItem, updateItem } from '../Slice';
import { Product, Unidad } from '../../reducers/types';
import {ProductScreens, ProductRootStackParamList} from '../screens';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import { HeaderButtons, ThemeColors } from '../../utils/theme';

const LOG_HIERACHY = 'Productos::Formulario -'; // LOG_HIERACHY,

type Props = NativeStackScreenProps<ProductRootStackParamList, ProductScreens.ProductForm>;
export default function FormularioProducto({navigation, route}: Props) {
  const dispatch = useAppDispatch();
  const product = useAppSelector(getFormItem);

  const isValid = useCallback(() => {
    try {
      return (
        product.nombre !== null &&
        product.nombre.trim() !== '' &&
        product.cantidadUnidad > 0 &&
        product.unidad.trim() !== ''
      );
    } catch (e) {
      console.error(e);
      return false;
    }
  }, [product]);

  const save = useCallback(
    async (prod: Product) => {
      if (!isValid()) {
        Alert.alert('Guardar', 'Datos invalidos');
        return;
      }

      prod.unidad ??= Unidad.Gramos;
      let operacion = prod.id ? ' actualizado ' : ' creado ';
      if (!prod.id) {
        dispatch(addItem(prod));
      } else {
        dispatch(updateItem(prod)).payload;
      }

      Toast.show({
        title:
          "Product '" + product.nombre + "'" + operacion + ' correctamente.',
        placement: 'top',
      });

      // if (route.params?.backTo) {
      //   console.log(LOG_HIERACHY, 'backTo =', route.params?.backTo);
      //   navigation.navigate({
      //     name: route.params.backTo,
      //     params: {product: true},
      //     merge: true,
      //   });
      // } else {
        if (navigation.canGoBack()) {
          navigation.goBack();
        }
      // }
    },
    [dispatch, product, navigation, isValid],
  );

  const update = useCallback(
    (prod: Product) => {
      dispatch(setFormItem(prod));
    },
    [dispatch],
  );

  useEffect(
    () =>
      navigation.setOptions({
        title: product.id ? 'Modificar producto' : 'Agregar producto',
        // eslint-disable-next-line react/no-unstable-nested-components
        headerRight: () => (
          <HStack backgroundColor={HeaderButtons.BackgroundColor}>
            <IconButton
              icon={<Icon name="save" size={30} color={HeaderButtons.Color} />}
              onPress={() => save(product)}
              testID="producto-save"
            />
          </HStack>
        ),
      }),
    [navigation, product, save],
  );

  console.log('Render FormularioProducto');
  return (
    <>
      <FormControl>
        <Stack mt="2" px="2">
          <FormControl.Label>Nombre</FormControl.Label>
          <Input
            testID="prod-name"
            autoFocus
            value={product?.nombre ?? ''}
            onChangeText={(nombre) => update(Object.assign({}, product, {nombre}))}
          />
        </Stack>
        <HStack justifyContent="center" mt="2" px="4" space={3}>
          <Stack width="50%">
            <FormControl.Label>Cantidad</FormControl.Label>
            <Input
              testID="prod-cantidad"
              keyboardType="number-pad"
              value={product?.cantidadUnidad + ''}
              onChangeText={(cantidadUnidad) =>
                update(Object.assign({}, product, {cantidadUnidad}))
              }
            />
          </Stack>
          <Stack width="50%">
            <FormControl.Label>Unidad</FormControl.Label>
            <Select
              selectedValue={product?.unidad?.toLowerCase()}
              onValueChange={(unidad: string) =>
                update(Object.assign({}, product, {unidad}))
              }
              testID="prod-unidad">
              <Select.Item label="Gramos" value="g" />
              <Select.Item label="Miligramos" value="mg" />
              <Select.Item label="Kilogramos" value="kg" />
              <Select.Item label="Mililitros" value="ml" />
              <Select.Item label="Litros" value="lt" />
              <Select.Item label="Paquetes" value="pq" />
              <Select.Item label="Unidades" value="u" />
            </Select>
          </Stack>
        </HStack>
        <Stack mt="2" px="2">
          <FormControl.Label>Codigo de barras</FormControl.Label>
          <Input
            testID="prod-barcode"
            keyboardType="number-pad"
            value={product?.barcode.toString()}
            onChangeText={(barcode) => update(Object.assign({}, product, {barcode}))}
          />
        </Stack>
        <Stack mt="2" px="2">
          <FormControl.Label>Observaciones</FormControl.Label>
          <Input
            testID="prod-observaciones"
            value={product?.observaciones}
            onChangeText={(observaciones) =>
              update(Object.assign({}, product, {observaciones}))
            }
          />
        </Stack>
      </FormControl>
    </>
  );
}

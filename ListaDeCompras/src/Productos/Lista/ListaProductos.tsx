import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import {
  View,
  HStack,
  Text,
  IconButton,
  Box,
  Pressable,
  VStack,
  useToast,
  useTheme,
} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Price, Product } from '../../reducers/types';
import { useAppDispatch, useAppSelector } from '../../reducers';
import { selectAll, setFormItem } from '../Slice';
import { selectAll as selectAllPrices } from '../Prices/Slice';
import Alert from '../../components/Alert';
import { RowMap, SwipeListView, SwipeRow } from 'react-native-swipe-list-view';
import { ProductScreens, ProductRootStackParamList } from '../screens';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { PriceScreens } from '../Prices/screens';
import { BarcodeScannerScreen } from '../../components/BarcodeScanner';
// import { HeaderOptionButton } from '../../components/HeaderOptionButton';
import { BoxListItemStyle, HeaderButtons, ThemeColors } from '../../utils/theme';
import ListItem from '../../components/ListItem';
import ListHiddenItems from '../../components/ListHiddenItems';

const LOG_HIERACHY = 'Productos::Lista - '; // LOG_HIERACHY,

type Props = NativeStackScreenProps<ProductRootStackParamList, ProductScreens.ProductList>;
function ListaProductos({ navigation, route }: Props) {
  const toast = useToast();
  const dispatch = useAppDispatch();
  const precios = useAppSelector(selectAllPrices);
  const productos = useAppSelector(selectAll);
  // const barcodeScanned = useAppSelector(getBarcodeScanned);
  const [isOpenCreateProduct, setIsOpenCreateProduct] = useState<boolean>(false);
  const [isOpenFoundProduct, setIsOpenFoundProduct] = useState<boolean>(false);
  const [productScanned, setProductScanned] = useState<Product>({} as Product);
  const openRowRef = useRef<SwipeRow<Product> | null>(null);

  const setDefaultProduct = useCallback(
    () => dispatch(setFormItem({ barcode: '' } as Product)),
    [dispatch],
  );

  const doScan = useCallback(() => {
    navigation.navigate(BarcodeScannerScreen.BarcodeScanner, {
      backTo: ProductScreens.ProductList,
    });
  }, [navigation]);

  const doAdd = useCallback(() => {
    setDefaultProduct();
    navigation.navigate(ProductScreens.ProductForm);
  }, [navigation, setDefaultProduct]);

  const handleSearch = useCallback(() => {
    toast.show({
      render: () => {
        return (
          <Box bg="emerald.500" px="2" py="1" rounded="sm" mb={5}>
            Implementar
          </Box>
        );
      },
    });
    // console.log(LOG_HIERACHY, 'handleSearch::text = ', text);
  }, [toast]);

  const headerRightButtons = useMemo(
    () => (
      <HStack backgroundColor={HeaderButtons.BackgroundColor}>
        <IconButton
          icon={<Icon name="barcode" size={30} color={HeaderButtons.Color} />}
          onPress={doScan}
        />
        <IconButton
          icon={<Icon name="search" size={30} color={HeaderButtons.Color} />}
          onPress={handleSearch}
        />
        <IconButton
          icon={<Icon name="plus" color={HeaderButtons.Color} size={30} />}
          onPress={doAdd}
        />
      </HStack>
    ),
    [doScan, handleSearch, doAdd],
  );

  useEffect(() => {
    navigation.setOptions({
      title: '',
      headerRight: () => headerRightButtons,
    });
  }, [navigation, headerRightButtons]);

  useEffect(() => {
    if (route.params?.barcodeScanned) {
      console.log(LOG_HIERACHY, 'route.params?.barcodeScanned = ', route.params?.barcodeScanned);
      const prod = productos.find((p: Product) => p.barcode === route.params?.barcodeScanned);

      if (prod) {
        setProductScanned(prod);
        setIsOpenFoundProduct(true);
        navigation.setParams({ barcodeScanned: undefined });
      } else if (!isOpenCreateProduct) {
        const newProduct = { barcode: route.params.barcodeScanned } as Product;
        dispatch(setFormItem(Object.assign({}, newProduct)));
        setIsOpenCreateProduct(true);
      }
    }
  }, [route, dispatch, productos]);

  const okPromptAddProduct = useCallback(() => {
    setIsOpenCreateProduct(false);
    // _editProduct(newProduct);
    // setBarcode(null);
    navigation.navigate(ProductScreens.ProductForm);
  }, [navigation]);

  const closeOpenRow = useCallback(() => {
    if (openRowRef.current && openRowRef.current.closeRow) {
      openRowRef.current.closeRow();
    }
  }, []);

  const _editProduct = useCallback(
    (producto: Product) => {
      closeOpenRow();
      setIsOpenFoundProduct(false);
      dispatch(setFormItem(Object.assign({}, {} as Product, producto)));
      navigation.navigate(ProductScreens.ProductForm);
    },
    [closeOpenRow, dispatch, navigation],
  );

  const _viewProduct = useCallback(
    (producto: Product) => {
      closeOpenRow();
      setIsOpenFoundProduct(false);
      dispatch(setFormItem(Object.assign({}, {} as Product, producto)));
      navigation.navigate(PriceScreens.Prices);
    },
    [closeOpenRow, dispatch, navigation, setIsOpenFoundProduct],
  );

  const renderItem = useCallback(
    ({ item }) => {
      const hasPrecio = precios.find((precio: Price) => precio.producto === item.id) !== undefined;
      const hasOferta =
        precios.find((precio: Price) => precio.producto === item.id && precio.oferta) !== undefined;

      const icons = (
        <>
          {hasPrecio ? <Icon name="money" size={20} color={ThemeColors.Text} /> : null}
          {hasOferta ? <Icon name="tags" size={20} color={ThemeColors.Text} /> : null}
        </>
      );

      return (
        <ListItem
          title={item.nombre}
          subtitle={item.cantidadUnidad + item.unidad}
          rightMain={icons}
        />
      );
    },
    [precios],
  );

  const renderItemEmptyList = useCallback(() => {
    return (
      <Box {...BoxListItemStyle}>
        <Text>No hay productos</Text>
      </Box>
    );
  }, []);

  const renderHiddenItem = useCallback(
    ({ item }) => {
      const icons = [
        {
          text: 'Edit',
          iconName: 'pencil',
          action: () => _editProduct(item),
        },
        {
          text: 'Prices',
          iconName: 'usd',
          action: () => _viewProduct(item),
        },
      ];
      return <ListHiddenItems icons={icons} />;
    },
    [_editProduct, _viewProduct],
  );

  const onRowDidOpen = useCallback((rowKey: string, rowMap: RowMap<Product>) => {
    openRowRef.current = rowMap[rowKey];
  }, []);

  // console.log('Render ListaProductos');
  return (
    <>
      <Alert
        title="Producto inexistente"
        isOpen={isOpenCreateProduct}
        text="El producto escaneado no existe. Desea agregarlo?"
        onCancel={() => {
          setIsOpenCreateProduct(false);
        }}
        onOk={okPromptAddProduct}
      />
      <Alert
        title="Seleccione"
        isOpen={isOpenFoundProduct}
        text={`Producto ${productScanned?.nombre}`}
        onOk={() => _viewProduct(productScanned)}
        okLabel="Ver"
        onThirdOption={() => _editProduct(productScanned)}
        thirdOptionLabel="Editar"
      />
      <SwipeListView
        data={productos}
        renderItem={renderItem}
        renderHiddenItem={renderHiddenItem}
        keyExtractor={(item) => item.id}
        rightOpenValue={-120}
        previewRowKey={'0'}
        previewOpenValue={-40}
        previewOpenDelay={10}
        onRowDidOpen={onRowDidOpen}
        disableRightSwipe={true}
      />
      {productos.length === 0 ? renderItemEmptyList() : null}
    </>
  );
}

export default ListaProductos;

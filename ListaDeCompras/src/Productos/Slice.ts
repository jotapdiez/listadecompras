import 'react-native-get-random-values';
import { v4 as uuidv4 } from 'uuid';
import { PayloadAction, createSlice  } from '@reduxjs/toolkit';
import { AppState } from '../reducers';
import { Product } from '../reducers/types';

interface ProductState {
    items: Product[],
    formItem: Product | null,
    fromScanner: string | null,
    status: string | null,
    error: string | null,
}

const initialState: ProductState = {
    items: [] as Product[],
    formItem: null,
    fromScanner: null,
    status: 'idle',
    error: null,
};

export const ENTITY = 'product';

const updateObject = (object: Product, updatedObject: Product) => {
    object.nombre = updatedObject.nombre;
    object.cantidadUnidad = updatedObject.cantidadUnidad;
    object.unidad = updatedObject.unidad;
    object.observaciones = updatedObject.observaciones;
    object.barcode = updatedObject.barcode;
    object.categoria = updatedObject.categoria;
    object.subcategoria = updatedObject.subcategoria;
    object.marca = updatedObject.marca;
    object.origen = updatedObject.origen;
    object.createDate = updatedObject.createDate;
    object.updateDate = updatedObject.updateDate;
};

export const ProductSlice = createSlice({
    name: ENTITY,
    initialState: initialState,
    reducers: {
        init: (state, action) => {
            state.items = action.payload;
        },
        setFormItem: (state, action: PayloadAction<Product>) => {
            state.formItem = Object.assign({nombre:'', unidad:'u', cantidadUnidad:1, observaciones:''}, action.payload);
        },
        addItem: (state, action: PayloadAction<Product>) => {
            const newItem = Object.assign({}, action.payload, { id: uuidv4() });
            state.items.push(newItem);
        },
        updateItem: (state, action: PayloadAction<Product>) => {
            state.status = 'done';
            const {id} = action.payload;
            const existingItem = state.items.find((item:Product) => item.id === id);
            if (existingItem) {
                updateObject(existingItem, action.payload);
            }
        },
        removeItem: (state, action: PayloadAction<Product>) => {
            state.status = 'done';
            const {id} = action.payload;
            const existingItem = state.items.find((item:Product) => item.id === id) || {} as Product;
            const existingItemIndex = state.items.indexOf(existingItem);
            if (existingItemIndex > -1) {
                const deletedItems = state.items.splice(existingItemIndex, 1);
                console.log('ProductSlice::removeItem=', deletedItems);
            }
        },
    },
});

export const { init, setFormItem, addItem, updateItem, removeItem } = ProductSlice.actions;

export const getStatus = (state: AppState):string => state[ENTITY].status;
export const getFormItem = (state: AppState): Product => state[ENTITY].formItem;
export const selectAll = (state: AppState): Product[] => state[ENTITY].items;
export const selectById = (state: AppState, itemId: string): Product => state[ENTITY].items.find((item: Product) => item.id === itemId);

export default ProductSlice.reducer;

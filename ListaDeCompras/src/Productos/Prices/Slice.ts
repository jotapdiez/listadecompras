import 'react-native-get-random-values';
import { v4 as uuidv4 } from 'uuid';
import { createSlice  } from '@reduxjs/toolkit';
import { AppState } from '../../reducers';
import { Price } from '../../reducers/types';

const initialState = {
    items: [] as Price[],
    formItem: null,
    status: 'idle',
    error: null,
};

export const ENTITY = 'price';

const updateObject = (object: Price, updatedObject: Price) => {
    object.tienda = updatedObject.tienda;
    object.producto = updatedObject.producto;
    object.valor = updatedObject.valor;
    object.moneda = updatedObject.moneda;
    object.oferta = updatedObject.oferta;
    object.tipoOferta = updatedObject.tipoOferta;
    object.createDate = updatedObject.createDate;
    object.updateDate = updatedObject.updateDate;
};

export const PriceSlice = createSlice({
    name: ENTITY,
    initialState: initialState,
    reducers: {
        init: (state, action) => {
            state.items = action.payload;
        },
        setFormItem: (state, action) => {
            state.formItem = action.payload;
        },
        updateFormItem: (state, action) => {
            state.formItem = action.payload;
        },
        addItem: (state, action) => {
            const newItem = Object.assign({}, action.payload, { id: uuidv4() });
            state.items.push(newItem);
        },
        updateItem: (state, action) => {
            state.status = 'done';
            const {id} = action.payload;
            const existingItem = state.items.find((item:Price) => item.id === id);
            if (existingItem) {
                updateObject(existingItem, action.payload);
            }
        },
        removeItem: (state, action) => {
            state.status = 'done';
            const {id} = action.payload;
            const existingItem = state.items.find((item:Price) => item.id === id) || {} as Price;
            const existingItemIndex = state.items.indexOf(existingItem);
            if (existingItemIndex > -1) {
                const deletedItems = state.items.splice(existingItemIndex, 1);
                console.log('PriceSlice::removeItem=', deletedItems);
            }
        },
    },
});

export const { init, setFormItem, updateFormItem, addItem, updateItem, removeItem } = PriceSlice.actions;

export const getStatus = (state: AppState):string => state[ENTITY].status;
export const getFormItem = (state: AppState):Price => state[ENTITY].formItem;
export const selectAll = (state: AppState):Price[] => state[ENTITY].items;
export const selectAllOfProduct = (state: AppState, productoId: string):Price[] => state[ENTITY].items.filter((item: Price) => item.producto === productoId);
export const selectById = (state: AppState, itemId: string):Price => state[ENTITY].items.find((item: Price) => item.id === itemId);

export default PriceSlice.reducer;

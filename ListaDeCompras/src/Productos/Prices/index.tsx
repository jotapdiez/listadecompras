import React, { useCallback, useEffect, useMemo } from 'react';
import { Box, HStack, IconButton, Text, View } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import { useAppDispatch, useAppSelector } from '../../reducers';
import { getFormItem as getProductFormItem } from '../Slice';
import { setFormItem } from './Slice';
import { Price } from '../../reducers/types';
import ListaPreciosPorValor from './ListaPreciosPorValor';
import { PriceScreens, PriceRootStackParamList } from './screens';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { getOptions } from '../../Options/Slice';
import { HeaderButtons, ThemeColors } from '../../utils/theme';

type Props = NativeStackScreenProps<PriceRootStackParamList, PriceScreens.Prices>;
const Prices = function ({ navigation }: Props) {
  const dispatch = useAppDispatch();

  const producto = useAppSelector(getProductFormItem);
  const options = useAppSelector(getOptions);

  const crearPrecio = useCallback(() => {
    dispatch(
      setFormItem(
        Object.assign(
          {},
          {
            tienda: options.defaultStore,
          } as Price,
          {
            producto: producto.id,
            moneda: 'ARS',
            tipoOferta: '',
          },
        ),
      ),
    );
    navigation.navigate(PriceScreens.PriceForm);
  }, [dispatch, producto, navigation, options]);

  const headerRightButtons = useMemo(
    () => (
      <HStack backgroundColor={HeaderButtons.BackgroundColor}>
        <IconButton
          icon={<Icon name="plus" size={30} color={HeaderButtons.Color} />}
          onPress={crearPrecio}
          testID="prices-add"
        />
      </HStack>
    ),
    [crearPrecio],
  );

  useEffect(() => {
    navigation.setOptions({
      title: `${producto.nombre} ${producto.cantidadUnidad} ${producto.unidad}`,
      headerRight: () => headerRightButtons,
    });
  }, [producto, headerRightButtons, navigation]);

  // console.log('Render ProducInfo');
  return (
    <>
      <Box pl={['1', '4']} pr={['1', '5']} py="1">
        <HStack p={3}>
          <Text>Ultima actualización:</Text>
          <Text ml={1}>
            {producto.updateDate ? moment.utc(producto.updateDate).format('L') : '-'}
          </Text>
        </HStack>
      </Box>
      <View>
        {/* <ListaPreciosPorTienda /> */}
        <ListaPreciosPorValor />
      </View>
    </>
  );
};

export default React.memo(Prices);

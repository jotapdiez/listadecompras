import React from 'react';
import { FlatList } from 'react-native';
import moment from 'moment';
import { useAppSelector } from '../../reducers';
import { getFormItem as getFormItemProducto } from '../../Productos/Slice';
import { selectAll as selectAllTiendas } from '../../Tiendas/Slice';
import { Price, Store } from '../../reducers/types';
import { selectAll } from './Slice';
import { Box, Text } from 'native-base';
import ListItem from '../../components/ListItem';
import { BoxListItemStyle, ThemeColors } from '../../utils/theme';

function ListaPreciosPorValor() {
  const producto = useAppSelector(getFormItemProducto);
  const allStores = useAppSelector(selectAllTiendas);
  const allPrices = useAppSelector(selectAll);

  const prices = React.useMemo(
    () =>
      allPrices
        .filter((item: Price) => item.producto === producto.id)
        // .sort((a: Price, b: Price) => parseInt(a.valor,2) - parseInt(b.valor,2))
        .sort(
          (a: Price, b: Price) =>
            moment.utc(a.createDate).toDate().getTime() +
            moment.utc(b.createDate).toDate().getTime(),
        ),
    [producto, allPrices],
  );

  if (prices.length === 0) {
    return (
      <Box {...BoxListItemStyle}>
        <Text>No hay precios para este producto.</Text>
      </Box>
    );
  }

  console.log('Render ListaPreciosPorValor');
  const renderListItem = (price: Price) => {
    const { createDate, moneda, valor } = price;
    return (
      <ListItem
        title={allStores.find((store: Store) => store.id === price.tienda)?.nombre ?? ''}
        subtitle={moment.utc(createDate).format('L')}
        rightMain={<Text>{moneda + ' ' + valor}</Text>}
      />
    );
  };

  return (
    <FlatList
      data={prices}
      renderItem={({ item }) => renderListItem(item)}
      keyExtractor={(item) => item.id.toString()}
    />
  );
}

export default React.memo(ListaPreciosPorValor);

export enum PriceScreens {
    Prices = 'PricesMain',
    PriceForm = 'FormularioPrecio',
    PricesByStoreList = 'ListaPreciosPorTienda',
    Options = 'Options',
}

export type PriceRootStackParamList = {
    [PriceScreens.Prices]: undefined;
    [PriceScreens.PriceForm]: undefined;
    [PriceScreens.PricesByStoreList]: undefined;
    Options: undefined;
};

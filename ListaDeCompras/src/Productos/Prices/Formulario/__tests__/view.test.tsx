import React from 'react';
import {screen, fireEvent} from '@testing-library/react-native';
import Formulario from '../Formulario';
import { renderComponents, validateAndChangeInputByDisplayValue, validateAndChangeInputByTestId, validateAndChangeSelectByTestId } from '../../../../__tests__/utils';
import store from '../../../../reducers';
import {ENTITY, setFormItem, init as initPrecios} from '../../Slice';
import {init as initProductos} from '../../../Slice';
import {init as initStores} from '../../../../Tiendas/Slice';
import {init as initOptions} from '../../../../Options/Slice';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import { createStackNavigator } from '@react-navigation/stack';
import { Options } from '../../../../reducers/types';
jest.mock('uuid', () => { return { v4: jest.fn(() => 'IDASD')}; });

const prices = [
    {
        id: 'a2b413aa-481d-436f-9e3e-121490ac1b47',
        createDate: '2019-07-13T20:01:10.186+00:00',
        updateDate: null,
        tienda: '69b6d343-035c-43c5-944a-deaa71b68deb',
        producto: '3b4fd0e2-7fc3-4e35-a2f3-3f2b0fda193f',
        valor: '300',
        moneda: '$',
        oferta: '3 o mas por 177,79',
        tipoOferta: null,
    },
];
const stores = [
    {
        id: '2255a704-02ea-4e43-af51-0ecf4e26dc2e',
        createDate: null,
        updateDate: null,
        pais: 'Argentina',
        ciudad: 'San Martin',
        nombre: 'Carrefour',
        direccion: 'Av. San Martín 421',
    },
    {
        id: '69b6d343-035c-43c5-944a-deaa71b68deb',
        createDate: null,
        updateDate: null,
        pais: 'Argentina',
        ciudad: 'Villa Maipú',
        nombre: 'Makro',
        direccion: 'Av. Gral. Paz',
    },
];
const products = [
    {
        id: '3b4fd0e2-7fc3-4e35-a2f3-3f2b0fda193f',
        createDate: '2020-09-22T03:16:08.287+00:00',
        updateDate: null,
        nombre: 'Gaseosa Pepsi',
        cantidadUnidad: 2.25,
        unidad: 'Lt',
        barcode: '7791813423119',
        categoria: 'Gaseosas',
        subcategoria: 'Gaseosa Cola',
        marca: 'Pepsi',
        origen: 'preciosmaximos',
        observaciones: 'obs...',
    },
];

beforeAll(() => {
    store.dispatch(initOptions({defaultCountry:'', defaultCity:'', defaultStore:''} as Options));
    store.dispatch(initStores(stores));
    store.dispatch(initProductos(products));
    store.dispatch(initPrecios(prices));
});

test('change value and update', () => {
    store.dispatch(setFormItem(prices[0]));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    validateAndChangeInputByDisplayValue('300', '150');

    fireEvent.press(screen.getByTestId('price-save'));
    const allItems = store.getState()[ENTITY].items;
    expect(allItems).toHaveLength(1);
    const producto = allItems[0];
    expect(producto.valor).toBe('150');
});

test('change moneda and update', () => {
    store.dispatch(setFormItem(prices[0]));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    validateAndChangeSelectByTestId('price-moneda', 'USD');

    fireEvent.press(screen.getByTestId('price-save'));
    const allItems = store.getState()[ENTITY].items;
    expect(allItems).toHaveLength(1);
    const producto = allItems[0];
    expect(producto.moneda).toBe('USD');
});

test('change store and update', () => {
    store.dispatch(setFormItem(prices[0]));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="Form" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    validateAndChangeSelectByTestId(
        'price-tienda',
        '2255a704-02ea-4e43-af51-0ecf4e26dc2e',
    );

    fireEvent.press(screen.getByTestId('price-save'));
    const allItems = store.getState()[ENTITY].items;
    expect(allItems).toHaveLength(1);
    const producto = allItems[0];
    expect(producto.tienda).toBe('2255a704-02ea-4e43-af51-0ecf4e26dc2e');
});

test('change offer and update', () => {
    store.dispatch(setFormItem(prices[0]));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="FormularioPrecio" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    validateAndChangeSelectByTestId('price-tipoOferta', 'masde4');

    // screen.getByTestId('store-save');
    fireEvent.press(screen.getByTestId('price-save'));
    const allItems = store.getState()[ENTITY].items;
    expect(allItems).toHaveLength(1);
    const producto = allItems[0];
    expect(producto.tipoOferta).toBe('masde4');
});

test('add new error no value', () => {
    store.dispatch(setFormItem({producto: products[0].id}));
    const Stack = createStackNavigator();
    renderComponents({
        store,
        ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
            <Stack.Screen name="FormularioPrecio" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>
        ),
    });

    // screen.debug();
    validateAndChangeInputByTestId('price-producto', products[0].id);

    fireEvent.press(screen.getByTestId('price-save'));
    const actualStores = store.getState()[ENTITY].items;
    expect(actualStores).toHaveLength(1);
});

test('add new error no store', () => {
    store.dispatch(setFormItem({producto: products[0].id}));
    const Stack = createStackNavigator();
    renderComponents({
        store,
        ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
            <Stack.Screen name="FormularioPrecio" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>
        ),
    });

    // screen.debug();
    validateAndChangeInputByTestId('price-producto', products[0].id);
    validateAndChangeInputByTestId('price-valor', 'valor');

    fireEvent.press(screen.getByTestId('price-save'));
    const actualStores = store.getState()[ENTITY].items;
    expect(actualStores).toHaveLength(1);
});

test('add new', () => {
    store.dispatch(setFormItem({producto: products[0].id}));
    const Stack = createStackNavigator();
    renderComponents({store, ui: (
        <SafeAreaProvider>
            <Stack.Navigator>
                <Stack.Screen name="FormularioPrecio" component={Formulario} />
            </Stack.Navigator>
        </SafeAreaProvider>)});

    // screen.debug();
    validateAndChangeInputByTestId('price-producto', products[0].id);
    validateAndChangeSelectByTestId('price-tienda', 'tienda');
    validateAndChangeInputByTestId('price-valor', 'valor');
    validateAndChangeSelectByTestId('price-moneda', 'moneda');
    // validateAndChangeInputByTestId('price-oferta', 'oferta');
    validateAndChangeSelectByTestId('price-tipoOferta', 'tipoOferta');

    fireEvent.press(screen.getByTestId('price-save'));
    const actualStores = store.getState()[ENTITY].items;
    expect(actualStores).toHaveLength(2);
    const currentStore = actualStores[1];
    expect(currentStore.producto).toBe(products[0].id);
    expect(currentStore.tienda).toBe('tienda');
    expect(currentStore.valor).toBe('valor');
    expect(currentStore.moneda).toBe('moneda');
    // expect(currentStore.oferta).toBe('oferta');
    expect(currentStore.tipoOferta).toBe('tipoOferta');
    expect(currentStore.id).toBe('IDASD');
});

import React, { useCallback, useEffect, useMemo } from 'react';
import { Select, Input, IconButton, FormControl, Stack, HStack, Toast } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useAppDispatch, useAppSelector } from '../../../reducers';
import { addItem, getFormItem, setFormItem, updateItem } from '../Slice';
import { selectAll as selectAllTiendas } from '../../../Tiendas/Slice';
import { Price, Product, Store } from '../../../reducers/types';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { selectAll as selectAllProducts } from '../../Slice';
import { PriceScreens, PriceRootStackParamList } from '../screens';
import { HeaderButtons, ThemeColors } from '../../../utils/theme';

type Props = NativeStackScreenProps<PriceRootStackParamList, PriceScreens.PriceForm>;
export default function FormularioPrecio({ navigation }: Props) {
  const dispatch = useAppDispatch();
  const precio = useAppSelector(getFormItem);
  const tiendas = useAppSelector(selectAllTiendas);
  const productos = useAppSelector(selectAllProducts);

  const save = useCallback(() => {
    if (!precio.valor) {
      Toast.show({
        title: 'Error',
        description: 'Debe ingresar un valor',
        placement: 'top',
      });

      return;
    }

    if (!precio.tienda || precio.tienda === '') {
      Toast.show({
        title: 'Error',
        description: 'Debe ingresar una Tienda',
        placement: 'top',
      });

      return;
    }

    precio.moneda ??= '$';
    // precio.producto ??= producto.id;

    if (!precio.tienda || precio.tienda === '') {
      if (tiendas.length > 0) {
        precio.tienda = tiendas[0].id;
      }
    }

    if (!precio.id) {
      dispatch(addItem(precio));
    } else {
      dispatch(updateItem(precio));
    }

    if (navigation.canGoBack()) {
      navigation.goBack();
    }
  }, [dispatch, navigation, precio, tiendas]);

  const headerRightButtons = useMemo(
    () => (
      <HStack backgroundColor={HeaderButtons.BackgroundColor}>
        <IconButton
          icon={<Icon name="save" color={HeaderButtons.Color} size={30} />}
          onPress={save}
          testID="price-save"
        />
      </HStack>
    ),
    [save],
  );

  useEffect(
    () =>
      navigation.setOptions({
        title: 'Agregar precio',
        headerRight: () => headerRightButtons,
      }),
    [headerRightButtons, navigation],
  );

  const update = useCallback(
    (obj: Price) => {
      let state = Object.assign({}, precio, obj);
      dispatch(setFormItem(state));
    },
    [dispatch, precio],
  );

  const producto = useMemo(() => {
    if (!precio || !precio.producto) {
      return null;
    }
    return productos.find((e: Product) => e.id === precio.producto);
  }, [precio, productos]);

  // console.log('Render FormularioPrecio');
  return (
    <FormControl>
      <Stack mt="2" px="2">
        <FormControl.Label>Producto</FormControl.Label>
        <Input
          testID="price-producto"
          placeholder="Producto"
          value={producto?.nombre}
          isDisabled
          variant="unstyled"
          editable={false}
        />
      </Stack>
      <HStack mt="2" px="2">
        <FormControl.Label width={'15%'}>Precio</FormControl.Label>
        <Select
          minWidth={'30%'}
          selectedValue={precio?.moneda}
          onValueChange={(moneda: string) => update(Object.assign({}, precio, { moneda }))}
          testID="price-moneda"
        >
          <Select.Item label="$" value="ARS" />
          <Select.Item label="U$S" value="USD" />
          <Select.Item label="E" value="EUR" />
        </Select>
        <Input
          testID="price-valor"
          placeholder="100"
          keyboardType="number-pad"
          width={'55%'}
          value={precio.valor}
          onChangeText={(valor) => update(Object.assign({}, precio, { valor }))}
        />
      </HStack>
      <Stack mt="2" px="2">
        <FormControl.Label>Tienda</FormControl.Label>
        <Select
          placeholder="Tienda"
          selectedValue={precio.tienda}
          onValueChange={(tienda: string) => update(Object.assign({}, precio, { tienda }))}
          testID="price-tienda"
        >
          {tiendas.map((item: Store) => (
            <Select.Item key={item.id} label={item.nombre} value={item.id + ''} />
          ))}
        </Select>
      </Stack>
      <Stack mt="2" px="2">
        <FormControl.Label>Tipo de oferta</FormControl.Label>
        <Select
          selectedValue={precio.tipoOferta}
          onValueChange={(tipoOferta: string) => update(Object.assign({}, precio, { tipoOferta }))}
          testID="price-tipoOferta"
        >
          <Select.Item label="Sin Oferta" value="" />
          <Select.Item label="3+" value="masde3" />
          <Select.Item label="4+" value="masde4" />
          <Select.Item label="Descuento 2 unidad (%)" value="desc2da" />
          <Select.Item label="Descuento 3 unidad (%)" value="desc3ra" />
          <Select.Item label="2x1" value="dospor1" />
          <Select.Item label="4x3" value="cutropor3" />
          <Select.Item label="6x4" value="seispor4" />
        </Select>
      </Stack>
    </FormControl>
  );
}

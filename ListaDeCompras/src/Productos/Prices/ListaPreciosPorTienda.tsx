import React from 'react';
import moment from 'moment';
import { List, Text, FlatList, Box, Pressable, HStack, Heading } from 'native-base';
// import filter from 'lodash/filter';
// import theme from '../components/Theme'
import { useAppSelector } from '../../reducers';
import { selectAll as selectAllTiendas } from '../../Tiendas/Slice';
// import {getFormItem as getFormItemProducto} from '../Productos/Slice';
import { Price, Store } from '../../reducers/types';
import { selectAll } from './Slice';

// type PricePerStore = {
//     id: number;
//     createDate: Date;
//     moneda: string;
//     valor: number;
//     oferta: string;
// };

export default function ListaPreciosPorTienda({ }) {
    const tiendas = useAppSelector(selectAllTiendas);
    // const producto = useAppSelector(getFormItemProducto);
    // const precios = useAppSelector((state) => selectPrecios(state, producto));
    const precios = useAppSelector(selectAll);

    const renderListItem = (price:Price) => {
        const {createDate, moneda, valor, oferta} = price;
        return (<Box pl={['1', '4']} pr={['1', '5']} py="1">
            <Pressable
                // onPress={_viewProduct.bind(this, item)}
                // onLongPress={_editProduct.bind(this, item)}
                rounded="8"
                overflow="hidden"
                borderWidth="1"
                borderColor="coolGray.300"
                shadow="3"
                bg="coolGray.100"
                p="5">
                <HStack space={[2, 3]} justifyContent="space-between">
                    <Text color="coolGray.800" bold>
                        {moneda + ' ' + valor}
                    </Text>
                    <Text color="coolGray.800" bold>
                        {oferta}
                    </Text>
                    <Text color="coolGray.800" bold>
                        {moment.utc(createDate).format('L')}
                    </Text>
                </HStack>
            </Pressable>
        </Box>);
    };

    const renderSectionHeader = (title) => {
        return (<Heading size="md" m="2">{title}</Heading>);
    };

    const groupBy = <T, K extends keyof any>(arr: T[], key: (i: T) => K) =>
    arr.reduce((groups, item) => {
        (groups[key(item)] ||= []).push(item);
        return groups;
    }, {} as Record<K, T[]>);

    const renderList = () => {
        const preciosPorTienda = groupBy(precios, (item: Price) => tiendas.find((t:Store) => t.id === item.tienda)?.nombre || 'SIN TIENDA');

        let items = [] as JSX.Element[];
        for (let storeName in preciosPorTienda) {
            const prices:Price[] = preciosPorTienda[storeName] || [] as Price[];
            items.push(
                <Box key={storeName}>
                    {renderSectionHeader(storeName)}
                    <FlatList
                        data={prices}
                        renderItem={({item}) => renderListItem(item)}
                        keyExtractor={(item, index) => item + '' + index}
                        />
                </Box>,
            );
        }

        return items;
    };

    return (<>
        {/* <FlatList data={productos} renderItem={renderItem} keyExtractor={item => item.id} /> */}
        <List>
            {renderList()}
        </List>
    </>);
}

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {ProductScreens} from './screens';

import Lista from './Lista';
import BarcodeScanner, { BarcodeScannerScreen } from '../components/BarcodeScanner';
import Prices from './Prices';
import Formulario from './Formulario';
// import ListaPreciosPorTienda from './Prices/ListaPreciosPorTienda';
import FormularioPrecio from './Prices/Formulario';

import {ProductRootStackParamList} from './screens';
import { PriceRootStackParamList, PriceScreens } from './Prices/screens';
import { NavigatorHeaderStyle } from '../utils/theme';

const Stack = createStackNavigator<ProductRootStackParamList & PriceRootStackParamList>();
export default function Products() {
  // console.log('Render ProductsNavigator');
  return (
    <Stack.Navigator screenOptions={NavigatorHeaderStyle}>
      <Stack.Screen name={ProductScreens.ProductList} component={Lista} />
      <Stack.Screen name={ProductScreens.ProductForm} component={Formulario} />
      <Stack.Screen name={BarcodeScannerScreen.BarcodeScanner} component={BarcodeScanner} />
      <Stack.Screen name={PriceScreens.Prices} component={Prices} />
      {/* <Stack.Screen
        name={ProductScreens.ListaPreciosPorTienda}
        component={ListaPreciosPorTienda}
      /> */}
      <Stack.Screen name={PriceScreens.PriceForm} component={FormularioPrecio} />
    </Stack.Navigator>
  );
}

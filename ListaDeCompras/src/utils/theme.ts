import { DefaultTheme, DarkTheme } from '@react-navigation/native';
import { extendTheme } from 'native-base';
import { StyleProp, TextStyle } from 'react-native';

// export const HeaderStyle = {
//     headerStyle: {
//         backgroundColor: 'rgb(8, 145, 178)',
//     },
//     headerTintColor: '#fff',
//     headerTitleStyle: {
//         fontWeight: 'bold',
//     } as StyleProp<TextStyle>,
// };

export enum ThemeColors {
  Primary = '#e1d5c9',
  Secondary = '#c19352',
  Action = '#ffd826',
  Text = 'black',
  TextLight = '#a3a3a3',
}

export const NativeBaseTheme = extendTheme({
  useSystemColorMode: true,
  initialColorMode: 'dark',
});

export const NavigatorTheme = {
  ...DarkTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: ThemeColors.Secondary,
    background: ThemeColors.Primary,
  },
};

export const HeaderButtons = {
  BackgroundColor: ThemeColors.Primary,
  Color: ThemeColors.Secondary,
};

export const NavigatorHeaderStyle = {
  headerStyle: {
    backgroundColor: HeaderButtons.BackgroundColor,
    borderWidth: 0,
  },
  headerTintColor: ThemeColors.Text,
  headerTitleStyle: {
    fontWeight: 'bold',
  } as StyleProp<TextStyle>,
};

export const BoxListItemStyle = {
  py: 2,
  // rounded:8,
  // borderWidth:1,
  // borderColor:'coolGray.300',
  //   borderBottomColor: 'black',
  //   borderBottomWidth: 1,
  // borderStyle: 'dashed',
  // borderBottomLeftRadius:200,
  // borderBottomRightRadius:200,
  bg: ThemeColors.Primary,
  px: 4,
  borderBottomColor: ThemeColors.Secondary,
  borderBottomWidth: 1,
};

// 2. Get the type of the CustomTheme
type CustomThemeType = typeof NativeBaseTheme;

// 3. Extend the internal NativeBase Theme
declare module 'native-base' {
  interface ICustomTheme extends CustomThemeType {}
}

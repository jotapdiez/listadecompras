import AsyncStorage from '@react-native-async-storage/async-storage';
import { STORAGE_ID, COMPRAS_STATE_NAME, TIENDAS_STATE_NAME, PRECIOS_STATE_NAME } from '../config';
import { ENTITY as ProductEntity } from '../Productos/Slice';
import { ENTITY as ShoppingListEntity } from '../ListaCompras/Slice';
import { ENTITY as StoreEntity } from '../Tiendas/Slice';
import { ENTITY as PriceEntity } from '../Productos/Prices/Slice';
import { ENTITY as OptionsEntity } from '../Options/Slice';
import { Price, Product, ShoppingList, Store } from '../reducers/types.js';

const COMPLETE_STORAGE_STR = STORAGE_ID + ':completeStore';
const DEFAULT_STORAGE = {
  [ProductEntity]: [
    {
      id: '3b4fd0e2-7fc3-4e35-a2f3-3f2b0fda193f',
      createDate: '2020-09-22T03:16:08.287+00:00',
      updateDate: null,
      nombre: 'Gaseosa Pepsi',
      cantidadUnidad: 2.25,
      unidad: 'Lt',
      barcode: '7791813423119',
      categoria: 'Gaseosas',
      subcategoria: 'Gaseosa Cola',
      marca: 'Pepsi',
      origen: 'preciosmaximos',
      observaciones: 'obs...',
    },
    {
      id: '5b8f0380-c411-471d-bfa0-2d977ed480a0',
      createDate: '2019-06-20T22:44:32.080+00:00',
      updateDate: null,
      nombre: 'Frutigran',
      cantidadUnidad: 250.0,
      unidad: 'mg',
      barcode: '7790045001188',
      categoria: '',
      subcategoria: '',
      marca: '',
      origen: '',
      observaciones: '',
      // },
      // {
      // 	'id': 'c22cb245-43cf-4976-bb5b-c9bcc5b23777',
      // 	'createDate': '2019-06-20T22:44:32.080+00:00',
      // 	'updateDate': null,
      // 	'nombre': 'Filtros Gizeh 6mm',
      // 	'cantidadUnidad': 120.0,
      // 	'unidad': 'u',
      // 	'barcode': '4002604034760',
      // 	'categoria': '',
      // 	'subcategoria': '',
      // 	'marca': '',
      // 	'origen': '',
      // 	'observaciones': '+30 filtros free',
    },
    {
      id: '35576969-78f1-409b-91bc-65cd5a66be8d',
      createDate: '2020-09-22T03:16:09.029+00:00',
      updateDate: '2019-07-13T20:01:30.493+00:00',
      nombre: 'Suavizante Ropa Violeta Doypack Vivere',
      cantidadUnidad: 3.0,
      unidad: 'Lt',
      barcode: '7791290013568',
      categoria: 'Limpieza de Ropa',
      subcategoria: 'Suavizante',
      marca: 'Vivere',
      origen: 'preciosmaximos',
      observaciones: '',
    },
    {
      id: '35576969-78f1-509c-16af-12ad1a11be1d',
      createDate: '2020-09-22T03:16:09.029+00:00',
      updateDate: '2019-07-13T20:01:30.493+00:00',
      nombre: 'Abadie',
      cantidadUnidad: 500,
      unidad: 'u',
      barcode: '8414775001155',
      categoria: 'Cigarrillos',
      subcategoria: 'Papeles',
      marca: 'Abadie',
      origen: 'unknown',
      observaciones: 'Papeles para cigarrillos',
    },
  ],
  [ShoppingListEntity]: [
    {
      id: '74f7beb5-24b9-436e-9ead-b301130ddfcf',
      createDate: '2019-06-20T22:44:32.080+00:00',
      updateDate: '2019-06-20T22:44:32.080+00:00',
      title: 'Mensual',
      productos: [
        {
          product_id: '3b4fd0e2-7fc3-4e35-a2f3-3f2b0fda193f',
          product_label: 'Pepsi 2.25lt',
          price_id: 'a2b413aa-481d-436f-9e3e-121490ac1b47',
          price_value: 300.0,
          cantidad: 4,
          comprados: 4,
        },
        {
          product_id: '5b8f0380-c411-471d-bfa0-2d977ed480a0',
          product_label: 'Frutigran 300g',
          price_id: null,
          price_value: 0.0,
          cantidad: 4,
          comprados: 2,
        },
        {
          product_id: '35576969-78f1-509c-16af-12ad1a11be1d',
          product_label: 'Abadie 500u',
          price_id: null,
          price_value: 0.0,
          cantidad: 4,
          comprados: 0,
        },
      ],
      fechaEstimadaCompra: '2019-06-28T23:51:49.112+00:00',
      importeCalculado: 350,
      ultimoCalculo: '2019-06-13T23:51:49.112+00:00',
    },
  ],
  [PriceEntity]: [
    {
      id: 'a2b413aa-481d-436f-9e3e-121490ac1b47',
      createDate: '2023-07-13T20:01:10.186+00:00',
      updateDate: null,
      tienda: '69b6d343-035c-43c5-944a-deaa71b68deb',
      producto: '3b4fd0e2-7fc3-4e35-a2f3-3f2b0fda193f',
      valor: 300,
      moneda: '$',
      oferta: '3 o mas por 177,79',
      tipoOferta: null,
    },
    {
      id: 'a1b413aa-481d-436f-9e3e-121490ac1b47',
      createDate: '2021-07-13T20:01:10.186+00:00',
      updateDate: null,
      tienda: '69b6d343-035c-43c5-944a-deaa71b68deb',
      producto: '3b4fd0e2-7fc3-4e35-a2f3-3f2b0fda193f',
      valor: 197.54,
      moneda: '$',
      oferta: 'por unidad',
      tipoOferta: null,
    },
    {
      id: 'a3b413aa-481d-436f-9e3e-121490ac1b47',
      createDate: '2022-07-13T20:01:10.186+00:00',
      updateDate: null,
      tienda: '2255a704-02ea-4e43-af51-0ecf4e26dc2e',
      producto: '3b4fd0e2-7fc3-4e35-a2f3-3f2b0fda193f',
      valor: 200.54,
      moneda: '$',
      oferta: null,
      tipoOferta: null,
    },
  ],
  [StoreEntity]: [
    {
      id: '2255a704-02ea-4e43-af51-0ecf4e26dc2e',
      createDate: null,
      updateDate: null,
      pais: 'Argentina',
      ciudad: 'San Martin',
      nombre: 'Carrefour',
      direccion: 'Av. San Martín 421',
    },
    {
      id: '69b6d343-035c-43c5-944a-deaa71b68deb',
      createDate: null,
      updateDate: null,
      pais: 'Argentina',
      ciudad: 'Villa Maipú',
      nombre: 'Makro',
      direccion: 'Av. Gral. Paz',
    },
    {
      id: 'd0a60094-87ce-4752-80d4-8a29d92dea80',
      createDate: null,
      updateDate: null,
      pais: 'Argentina',
      ciudad: 'San Martin',
      nombre: '(Chino) Prospero',
      direccion: 'República del Líbano 4251',
    },
    {
      id: '64d4d6b2-bd1e-4c8b-8479-35bb11dd34de',
      createDate: null,
      updateDate: '2019-06-20T22:44:32.080+00:00',
      pais: 'Argentina',
      ciudad: 'San Martin',
      nombre: '(Chino) Imperial',
      direccion: 'De Moussy 4747',
    },
  ],
  [OptionsEntity]: {
    defaultServer: 'San Martin',
    defaultCity: 'San Martin',
    defaultCountry: 'argentina',
    defaultStore: '69b6d343-035c-43c5-944a-deaa71b68deb', // Makro
    // defaultStore:'2255a704-02ea-4e43-af51-0ecf4e26dc2e', // Carrefour
  },
  reduxState: {},
};

export class AsyncStorageUtils {
  currentStore: { reduxState: {} };
  constructor() {
    this.currentStore = {
      reduxState: {},
    };
  }

  createDefaultStorage = () => {
    let json = JSON.stringify(DEFAULT_STORAGE);
    this._saveFullStorage(json);
    return DEFAULT_STORAGE;
  };

  _saveFullStorage = (storage) => {
    //TODO: Comprobar si hay cambio posteriores a la fecha del store y realizar un merge para no perder cambios locales
    if (!storage.reduxState) {
      storage.reduxState = {};
    }

    AsyncStorage.setItem(COMPLETE_STORAGE_STR, storage);
    // DEFAULT_STORAGE
  };

  save = (name, value) => {
    this.currentStore[name] = value;
    const storingValue = JSON.stringify(this.currentStore);
    this._saveFullStorage(storingValue);
  };

  getFullStore = () => {
    return this.currentStore;
  };

  load = async () => {
    try {
      let json = await AsyncStorage.getItem(COMPLETE_STORAGE_STR);
      // console.log('AsyncStorageUtils::load::json: ', json);
      if (json == null) {
        this.currentStore = this.createDefaultStorage();
      } else {
        this.currentStore = JSON.parse(json);
      }
    } catch (e) {
      console.log('Error oteniendo storage. ** Guardando DEFAULT **', e);
      this.currentStore = this.createDefaultStorage();
    }

    return this.currentStore;
  };

  clear = () => {
    AsyncStorage.clear((error) => {
      if (error) {
        console.error('AsyncStorageUtils::clearStorage::error: ', error);
      } else {
        console.log('AsyncStorageUtils::clearStorage DONE!!');
        this.setHardcodedStorage();
      }
    });
  };

  setHardcodedStorage = () => {
    console.log('AsyncStorageUtils::setHardcodedStorage');
    let json = JSON.stringify(DEFAULT_STORAGE);
    this._saveFullStorage(json);
    // this.save('tiendas', []);
  };

  persistTiendas = (tiendas: Store) => {
    this.save(TIENDAS_STATE_NAME, tiendas);
  };

  persistProductos = (prods: Product[]) => {
    this.save(ProductEntity, prods);
  };

  persistListas = (listas: ShoppingList) => {
    this.save(COMPRAS_STATE_NAME, listas);
  };

  persistPrecios = (precio: Price) => {
    this.save(PRECIOS_STATE_NAME, precio);
  };
}

export default new AsyncStorageUtils();

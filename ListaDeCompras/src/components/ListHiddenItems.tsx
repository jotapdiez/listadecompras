import React from 'react';
import { Box, Pressable, Text, VStack } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ThemeColors } from '../utils/theme';

type IconPros = {
  text: string;
  iconName: string;
  action: () => void;
};

type ListHiddenItemsProps = {
  icons: IconPros[];
};

function ListHiddenItems({ icons }: ListHiddenItemsProps) {
  // console.log('Rendering ListHiddenItems');

  const iconsComponent = icons.map((item, index) => (
    <Pressable
      key={item.iconName + index}
      p={2}
      _pressed={{ opacity: 0.5 }}
      onPress={item.action}
      borderColor={'black'}
      // borderWidth={1}
    >
      <VStack alignItems={'center'} backgroundColor={ThemeColors.Secondary}>
        <Icon name={item.iconName} color={ThemeColors.Action} />
        <Text fontWeight="medium" color={ThemeColors.Action}>
          {item.text}
        </Text>
      </VStack>
    </Pressable>
  ));

  return (
    <Box
      flex={1}
      flexDirection={'row'}
      justifyContent={'flex-end'}
      alignItems={'center'}
      justifyItems={'space-evenly'}
      backgroundColor={ThemeColors.Secondary}
      pr={2}
    >
      {iconsComponent}
    </Box>
  );
}

export default React.memo(ListHiddenItems);

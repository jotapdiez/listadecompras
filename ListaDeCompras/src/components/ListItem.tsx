import { Box, HStack, Pressable, Text, VStack } from 'native-base';
import React, { ReactElement } from 'react';
import { BoxListItemStyle, ThemeColors } from '../utils/theme';

type ListItemProps = {
  title: string | number | ReactElement;
  subtitle?: string | number | ReactElement;
  rightMain?: string | number | ReactElement;
  rightSub?: string | number | ReactElement;
};

function ListItem({ title, subtitle, rightMain, rightSub }: ListItemProps) {
  // console.log('Rendering ListItem');
  return (
    <Box
    {...BoxListItemStyle} borderBottomColor={ThemeColors.Secondary} borderBottomWidth={1}>
      <Pressable overflow="hidden">
        <HStack justifyContent="space-between">
          <VStack justifyContent="space-between" flex={1}>
            <Text color={ThemeColors.Text} bold>
              {title}
            </Text>
            <Text color={ThemeColors.TextLight} fontSize="xs">
              {subtitle}
            </Text>
          </VStack>
          <VStack justifyContent={'space-evenly'} alignItems={'flex-end'} minWidth={'15%'}>
            {rightMain}
            {rightSub}
          </VStack>
        </HStack>
      </Pressable>
    </Box>
  );
}

export default React.memo(ListItem);

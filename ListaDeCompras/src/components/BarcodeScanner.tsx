import React, { useEffect, useRef, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { useCameraDevices, Camera, Frame, useFrameProcessor } from 'react-native-vision-camera';
import { BarcodeFormat, scanBarcodes } from 'vision-camera-code-scanner';
import { Text } from 'native-base';
import { useAppDispatch } from '../reducers';
import {setBarcodeScanned} from '../reducers/BarcodeSlice';

import { runOnJS } from 'react-native-reanimated';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

const LOG_HIERACHY = 'Components::BarcodeScanner - '; // LOG_HIERACHY,

export function useCustomScanBarcodes(): [(frame: Frame) => void, string | null] {
  const [barcodes, setBarcodes] = useState(null);
  const previousBarcode = useRef<string | null>(null);

  const updateBarcodes = (data: string | any[]) => {
    if (data.length === 0 ||
      (previousBarcode.current && data[0].displayValue === previousBarcode.current)) {
      return;
    }

    // console.log(LOG_HIERACHY, 'barcodes', barcodes);
    // console.log(LOG_HIERACHY, 'previousBarcode.current', previousBarcode.current,);
    // console.log(LOG_HIERACHY, 'updateBarcodes', data[0].displayValue);
    previousBarcode.current = data[0].displayValue;
    setBarcodes(data[0].displayValue);
  };

  const frameProcessor = useFrameProcessor((frame) => {
    'worklet';
    const detectedBarcodes = scanBarcodes(frame, [BarcodeFormat.ALL_FORMATS], {
      checkInverted: true,
    });
    runOnJS(updateBarcodes)(detectedBarcodes);
  }, []);

  return [frameProcessor, barcodes];
}

export enum BarcodeScannerScreen {
  BarcodeScanner = 'BarcodeScanner',
}

export type BarcodeScannerStackParamList = {
  [BarcodeScannerScreen.BarcodeScanner]: {backTo: string} | undefined;
};

type Props = NativeStackScreenProps<BarcodeScannerStackParamList, BarcodeScannerScreen.BarcodeScanner>;
export default function BarcodeScannerComponent({navigation, route}: Props) {
  const dispatch = useAppDispatch();
  const [isScanned] = React.useState(false);

  const devices = useCameraDevices();
  const device = devices.back;

  const [frameProcessor, barcodes] = useCustomScanBarcodes();

  useEffect(() => {
    navigation.setOptions({
      title: 'Escanear producto',
    });
  }, [navigation]);

  useEffect(() => {
    async function perm() {
      const currentCameraPermission = await Camera.requestCameraPermission();
      // console.log(LOG_HIERACHY, 'currentCameraPermission=', currentCameraPermission)
      if (currentCameraPermission === 'denied') {
        const newCameraPermission = await Camera.requestCameraPermission();
        console.log(LOG_HIERACHY, 'newCameraPermission=', newCameraPermission);
      }
      // const newMicrophonePermission = await Camera.requestMicrophonePermission()
      // console.log(LOG_HIERACHY, 'newMicrophonePermission=', newMicrophonePermission)
    }

    perm();
  }, []);

  useEffect(() => {
    console.log(LOG_HIERACHY, 'useEffect barcodes=', barcodes);
    if (barcodes == null) {
      return;
    }
    // if (barcodes.length > 0) {
    //   barcodeReceived(barcodes[0].displayValue);
    // }
    console.log(LOG_HIERACHY, 'backTo =', route.params?.backTo);
    if (route.params?.backTo) {
      navigation.navigate({
        name: route.params.backTo,
        params: {barcodeScanned: barcodes},
        merge: true,
      });
    } else {
      dispatch(setBarcodeScanned(barcodes));
      navigation.goBack();
    }
    // barcodeReceived(barcodes);
    // return () => {
    //   barcodes;
    // };
  }, [barcodes, dispatch, navigation, route]);

  if (device == null) {
    return (
      <View style={{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <Text>Cargando...</Text>
      </View>
    );
  }

  console.log(LOG_HIERACHY, 'Render BarcodeScanner');
  return (
    <>
      <View style={styles.container}>
        <Camera
          style={StyleSheet.absoluteFill}
          device={device}
          isActive={!isScanned}
          // isActive={true}
          frameProcessor={frameProcessor}
          frameProcessorFps={5}
          audio={false}
          // onBarCodeRead={barcodeReceived}
          // style={{ flex: 1 }}
          // captureAudio={false}
          // torchMode={TORCH_MODE}
          // cameraType={CAMERA_TYPE}
        />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  statusBar: {
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  statusBarText: {
    fontSize: 20,
  },
});

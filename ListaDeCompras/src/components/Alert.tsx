import { AlertDialog, Button } from 'native-base';
import React from 'react';
import { ThemeColors } from '../utils/theme';

type AlertProps = {
  title: string;
  text: string;
  isOpen: boolean;
  onCancel?: () => void;
  cancelLabel?: string;
  onOk: () => void;
  okLabel?: string;
  onThirdOption?: () => void;
  thirdOptionLabel?: string;
};

export default function Alert({
  title,
  text,
  isOpen,
  onCancel,
  cancelLabel,
  onOk,
  okLabel,
  onThirdOption,
  thirdOptionLabel,
}: AlertProps) {
  // const [isOpen, setIsOpen] = React.useState(true);
  const cancelRef = React.useRef(null);
  const onClose = () => {}; //setIsOpen(false);

  const renderThirdOption = () => {
    if (!onThirdOption) {
      return <></>;
    }
    return (
      <Button colorScheme="danger" onPress={onThirdOption}>
        {thirdOptionLabel || 'Delete'}
      </Button>
    );
  };

  return (
    <AlertDialog leastDestructiveRef={cancelRef} isOpen={isOpen} onClose={onClose}>
      <AlertDialog.Content>
        <AlertDialog.CloseButton />
        <AlertDialog.Header>{title}</AlertDialog.Header>
        <AlertDialog.Body>{text}</AlertDialog.Body>
        <AlertDialog.Footer>
          <Button.Group space={2}>
            {onCancel && (
              <Button variant="unstyled" colorScheme="coolGray" onPress={onCancel} ref={cancelRef}>
                {cancelLabel || 'Cancel'}
              </Button>
            )}
            {onOk && (
              <Button colorScheme="green" onPress={onOk}>
                {okLabel || 'Ok'}
              </Button>
            )}
            {renderThirdOption()}
          </Button.Group>
        </AlertDialog.Footer>
      </AlertDialog.Content>
    </AlertDialog>
  );
}

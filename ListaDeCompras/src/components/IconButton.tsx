import React from 'react';
import { IconButton } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

interface CustomIconButtonProps {
    icon: string;
    size?: number;
    color?: string;
    onPress: () => void;
    testID?: string;
}

export default function CIconButton({icon, size = 20, color = 'black', onPress, testID}: CustomIconButtonProps) {
    return (
        <IconButton
            icon={<Icon name={icon} size={size} color={color} />}
            onPress={onPress}
            testID={testID}
        />
    );
}

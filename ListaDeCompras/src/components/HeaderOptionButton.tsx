import { IconButton, useTheme } from 'native-base';
import React from 'react';
import { OptionScreen } from '../Options';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useNavigation } from '@react-navigation/native';

type HeaderOptionButtonProps = {};

export const HeaderOptionButton = React.memo(({}: HeaderOptionButtonProps) => {
  const navigation = useNavigation();
  const { colors } = useTheme();

  const showOptions = () => {
    navigation.navigate(OptionScreen.Options);
  };

  return (
    <IconButton
      icon={<Icon name="cog" size={30} color={colors.dark[400]} />}
      onPress={showOptions}
    />
  );
});

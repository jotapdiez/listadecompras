import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { HStack, Text, IconButton } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

export type HeaderProps = {
    title: string;
    leftIcon?: string;
    leftIconOnPress?: Function;
    children?: React.ReactNode;
};

const Header = React.memo(({ title, leftIcon, leftIconOnPress, children }: HeaderProps) => {
// const Header = ({ title, leftIcon, leftIconOnPress, children }: HeaderProps) => {
    const navigation = useNavigation();

    const handleLeftIconOnPress = () => {
        if (leftIconOnPress) {
            leftIconOnPress();
        } else {
            if (navigation.canGoBack()) {
                navigation.goBack();
            }
        }
    };

    // console.log('Render Header');
    return (
        <HStack
            bg={'primary.600'}
            justifyContent="space-between"
            w="100%">
            <HStack alignItems="center">
                <IconButton
                    icon={
                    <Icon
                        size={30}
                        name={leftIcon ? leftIcon : 'arrow-left'}
                        onPress={handleLeftIconOnPress}
                        color="white"
                    />
                    }
                />
                <Text color="white" fontSize="20" fontWeight="bold">
                    {title}
                </Text>
            </HStack>
            <HStack>{children}</HStack>
        </HStack>
    );
}, (prevProps, nextProps) => {
    return prevProps.title === nextProps.title
        && prevProps.children === nextProps.children;
});
// };
export default Header;

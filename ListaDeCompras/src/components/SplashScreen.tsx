import React from 'react';
import {Center, Text} from 'native-base';

export default function SplashScreen({}) {
  return (
    <>
      <Center w="100%" h="95%">
        <Text
          fontSize="5xl"
          _light={{
            color: 'primary.500:alpha.30',
          }}>
          Lista
        </Text>
        <Text
          fontSize="5xl"
          _light={{
            color: 'secondary.500:alpha.70',
          }}>
          de
        </Text>
        <Text
          fontSize="5xl"
          _light={{
            color: 'tertiary.500:alpha.70',
          }}>
          Compras +
        </Text>
      </Center>
      <Center w="100%" h="5%">
        <Text
          _light={{
            color: 'emerald.500',
          }}
          _dark={{
            color: 'emerald.400',
          }}>
          Cargando...
        </Text>
      </Center>
    </>
  );
}

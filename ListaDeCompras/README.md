# LDC+ Mobile App

Aplicacion para listas de compras

## Idea

TODO:

## Build IOS:

- Compilar con: `cd ios/ && xcodebuild -workspace ListaDeCompras.xcworkspace -configuration Debug -scheme ListaDeCompras -destination id=3713EDDD-F885-4648-A9E8-16D2F92544A1`
- Si termina con `** BUILD SUCCEEDED **`, ejecutar `cd .. && yarn run ios`

## Build Android

`cd android && ./gradlew clean && cd .. && yarn android`

## Implementado:

TODO:

## TODO

- AM de Listas de compra
- En AsyncStorageUtils, en Prices, si createDate es new Date('2023-07-13T20:01:10.186+00:00') en lugar de '2023-07-13T20:01:10.186+00:00' da error redux
- Sincronizar precios seleccionando zona para no descargar toda la DB (Front y Back)
- Sistema de evento para notificar al backend de:
  - Nuevos precios
  - Nuevos productos
- Implementar un theme + dark mode
- Wizard de configuracion inicial
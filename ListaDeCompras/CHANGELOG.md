# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.11] - 2023-05-02
### Changed
- Configurado ESlint y prettier
- Ajustes en el modulo de IOS
- Muchos ajustes de Theme (colores), prettier/eslint, typescript
- Renombrados algunos archivos

## [0.0.10] - 2023-04-22
### Changed
- Arreglados los UT por cambio de el Header
- Validaciones en los formularios de Precio, Producto y Tienda
- Arreglos de TS
- Arreglo en el guardado de las modificaciones a los productos de la Lista de compras
- Limpieza de codigo, console.log y comentarios


## [0.0.9] - 2023-04-20
### Changed
- Eliminado el componente Header para usar el header de ReactNative navigation
- Agregado Redux para Options utilizado para defaults
- Cambiado ToastAndroid por useToast de NativeBase
- Cambios varios por performance

## [0.0.8] - 2023-04-19
### Changed
- Implementada la funcionalidad de agregar productos inexistentes a la ShoppingList mediante scaneo y agregado
- Cambios varios por Linting
- Eliminados varios archivos no TS
- Cambios varios por performance
- Cambios varios esteticos
- Utils y base para UTs

## [0.0.7] - 2023-03-24
### Changed
- Implementada la funcionalidad de interactuar con los productos de la ShoppingList
- Arreglos de TS

## [0.0.6] - 2023-03-16
### Changed
- IOS funcionando (en emulador)

## [0.0.5] - 2023-03-08
### Changed
- Implementadas el update y agregado de Precios
- Agregado el ID al insertar nueva entidad (producto, precio, etc)
- Arreglados los UT que estaban mal y agregados algunos

## [0.0.4] - 2023-03-07
### Changed
- Implementadas las listas por Tienda y por Valor de los precios para un producto.

## [0.0.3] - 2023-03-07
### Changed
- Implementada la funcionalidad de escanear codigo de barra permitiendo agregar producto si no existe o editarlo/verlo si existe.

## [0.0.2] - 2023-02-27
### Changed
- Pasado todo a TS
- Pasado a redux-toolkit
- Cambios esteticos
- Eliminados archivos/componentes sin uso
- Eliminados los UT existentes
- Configurado para nuevos UT
- Agregado UT de ejemplo (WIP) en Productos/Formulario
- Utils para UTs

## [0.0.1] - 2023-02-27
### Changed
- Actualizadas todas las libs
- Actualizadas todos los archivos de java/gradle para android
- Modificaciones varias por los updates de las libs
- Eliminada carpeta ListaDeComprasReact que no me acuerdo de que era
- Version basica funcionando en Android (listas, formularios, escaneo de productos)
- Agregado README (con TODO) y este CHANGELOG

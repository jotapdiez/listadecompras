module.exports = {
  presets: [
    'module:metro-react-native-babel-preset',
    '@babel/preset-react',
    // '@babel/preset-flow',
    // '@babel/preset-typescript',
  ],
  plugins: [
    '@babel/plugin-syntax-jsx',
    [
      'react-native-reanimated/plugin',
      {
        relativeSourceLocation: true,
        globals: ['__scanCodes'],
      },
    ],
  ],
};

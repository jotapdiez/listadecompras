module.exports = {
  env: {
    es6: true,
    node: true,
    jest: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
  ],
  parserOptions: {
    project: './ListaDeCompras/tsconfig.json',
  },
  plugins: ['react', 'react-hooks', '@typescript-eslint'],
  root: true,
  extends: '@react-native-community',
  "plugins": [
    "react"
  ],
  rules: {
    'prettier/prettier': 'error',
    "react/react-in-jsx-scope": "off",
    // indent: ['error', 2, { SwitchCase: 1 }],
    quotes: ['error', 'single', { avoidEscape: true }],
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
};

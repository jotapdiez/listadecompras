import 'react-native-reanimated';
import 'react-native-gesture-handler';
import MainScreen from './src/MainScreen';
import {AppRegistry} from 'react-native';
// import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => MainScreen);

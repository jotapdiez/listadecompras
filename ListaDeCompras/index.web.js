import React from 'react';
import './index.css';
// import App from './src';
import * as serviceWorker from './serviceWorker';
import { AppRegistry, View, Text } from 'react-native';

function App() {
    return (
        <View>
            <Text>Test</Text>
        </View>
    );
}

// ReactDOM.render(
//     <React.StrictMode>
//         <View>
//       <Text>Test</Text>
//     </View>
//         {/* <App /> */}
//     </React.StrictMode>,
//     document.getElementById('root')
// );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
AppRegistry.registerComponent('App', () => App);
AppRegistry.runApplication('App', { rootTag: document.getElementById('react-root') });
